Orgelet is a WebGL geographical quiz game. It is my personal project to learn WebGL as well as try different approaches to organizing a full-stack Typescript application.

This project uses git-flow (`main`, `develop` & `feature` branches), albeit very loosely since it is a one-person project in prototyping phase.

![Preview](docs/orgelet-loop.mp4)

# Goals
- Do a 100% Typescript project
- Have a completely WebGL-based browser interface
- Support both offline/singleplayer and online/multiplayer gameplay
- Modularize the code (albeit in a single repository)
- Experiment with backend and data storage ([GraphQL](https://graphql.org/)/[typeorm](https://typeorm.io/)...)
- Experiment with React state management
- Experiment with CI/CD (docker)

# Structure
The code is modularized into:

- **orgelet-core**
  Common data structures and business/game logic
- **orgelet-countries**
  Scripts for pre-rendering world maps from
  [Naturalearth](http://www.naturalearthdata.com/) and extracting countries' data
  needed for the game.
- **orgelet-api**
  Backend [GraphQL](https://graphql.org/) API for postgresql database (via [typeorm](https://typeorm.io/))
- **orgelet-app**
  React frontend app, using [Pixi.js](https://www.pixijs.com/) for WebGL
- **orgelet-db**
  Contains dockerfile and initialization scripts for the database container (based on `postgres:12` docker image)

# Setup
1. Clone the repository
2. Create a `secret/development.env` and set these values:
```
# Postgres root user (used to set up orgelet-db)
POSTGRES_USER=
POSTGRES_PASSWORD=

# Backend postgres settings (used by orgelet-api and to set up orgelet-db)
TYPEORM_USERNAME=
TYPEORM_PASSWORD=
TYPEORM_DATABASE=
```
3. Generate key for JWT: `ssh-keygen -t rsa -b 4096 -m PEM -f secret/orgelet-jwt-key`
4. Run docker-compose: `yarn dc:up:dev` (this can take a couple of minutes)
5. Run typeorm migrations: `yarn migration:run:dev`
6. The app endpoint is [http://localhost:8080](http://localhost:8080) and the GraphQL API on [http://localhost:3000/graphql](http://localhost:3000/graphql)

# Lessons learned
This is an early prototype and the project is not nearly finished, but I have managed to learn some things on the way:

### React

#### Pixi.js
React might not be the best choice for dealing with WebGL. Using a web graphics library (such as [Pixi.js](https://www.pixijs.com/)) in combination with React also might unnecessarily complicate things. Handling user interactions, specially on a higher level (text input, mouse gestures...) is sometimes way easier with classic HTML/DOM. Several questions arose during development, mainly regarding accessibility, adherence to browser best practices, native look-and-feel...

[@inlet/react-pixi](https://github.com/inlet/react-pixi) is easy to use and fits well into (modern) React, but depends on outdated [Pixi.js](https://www.pixijs.com/) and provides own Typescript type definitions (which have since started being shipped officially by the Pixi.js project).


#### State management
Even though I used [redux-saga](https://redux-saga.js.org/) at first, since it is appropriate for implementing readable and intuitive game-loops (and I am used to it), I am currently experimenting with pure React hooks approach in combination with [Apollo local state management](https://www.apollographql.com/docs/tutorial/local-state/).

The app part of the project right now is in a refactoring mess to remove Redux. Game logic is running locally and no communication to the backend is implemented from the React app.


### Naturalearth data
Maps are rendered by a headless script ([node-pixi](https://github.com/timknip/node-pixi) is outdated and uses older Pixi.js engine based on canvas instead of WebGL). I haven't managed to do proper map shading, but am quite satisfied with the result. [Turf.js](http://turfjs.org/) was used extensively for geospatial purposes.

### Modularization
Modularization of the project is done via [Yarn Workspaces](https://classic.yarnpkg.com/en/docs/workspaces/). I thought about using [Lerna](https://github.com/lerna/lerna) but that might be an overkill. The only drawback with Yarn Workspaces was that I had to rewire the React app in order for it to be able to require dependencies from the Workspaces' `node_modules` folder.

In earlier experimental projects I used a private NPM repository (on Gitlab) and separated the modules into their own git repositories. While possible and appropriate for certain use cases, single repository with Workspaces is much more practical.

### GraphQL
[GraphQL](https://graphql.org/) is used for the API with [typeorm](https://typeorm.io/) and postgres. Web Tokens are used for authentication (although currently just to provide sessions, without registration/login functionality). This part of the project is still very alpha.

### Docker
Docker and docker-compose was successfully used to set up development and test environments. I don't have much experience with docker and am not really confident in current implementation. At first there wre only two containers (app/db) but later it turned out that modularizing applies nicely to docker as well, making small changes in higher level modules faster to build (but changing e.g. orgelet-core package.json or Dockerfile will require rebuilding most of the images).

Unit tests were only sprinkled here and there to check the test docker environment, no real CI/CD as of yet. I have been testing Gitlab because of it's CI/CD functionality (since before Github Actions were stable). For this project I was running a Gitlab Docker Runner on my laptop. When a new commit is pushed to the repository, Gitlab will communicate with the Runner on my laptop to run the tests. This enables me to use docker images for testing without the need for a private Docker repository in early development. Docker repository would however be a requirement in production phase in order to completely integrate CD.

This project is also a victim of less than stellar practice of passing secrets as environment variables!

### Typescript

#### Typescript & React
React (functional components & hooks approach) plays better with Typescript than I expected. I also managed to make React HOCs without too much hassle.

Why even use HOCs if we have hooks? Exactly! For WebGL transitions (animations) I ended up implementing a hook and only wrapped it inside HOC for easier usage when transitions need to happen in a component that is already unmounted. HOC wraps the component that should already have unmounted, but the wrapper delays the unmounting until the (e.g. fade-out) transition is finished.

TODO link to code packages/orgelet-app/src/view/components/atoms/Transition


#### Typescript & 3rd party libraries
There are caveats with using Typescript with some libraries. Since [Ramda](https://ramdajs.com/) and [Turf.js](http://turfjs.org/) are very functional-oriented, certain modules are on the frontier of Typescript functionality and some just simply don't have type definitions because they would be too convoluted. This can be a real show-stopper! The orgelet-countries package was first written in JS and then unsuccessfully ported to Typescript and is currently in a non-runnable superposition...

At first I chose [Sequelize](https://sequelize.org/) as the ORM but it turned out it is really not Typescript ready, so I switched to [typeorm](https://typeorm.io/).

#### Typescript & GraphQL
The whole [GraphQL](https://graphql.org/)/[type-graphql](https://github.com/MichalLytek/type-graphql)/[typeorm](https://typeorm.io/) combination is a bit questionable. What exactly should the source specification be for the models? type-graphql approach (classes with ES decorators as a source for generating GraphQL schemas) looks great and plays well with typeorm but can't be used on the frontend. Core model definitions for this project right now are actually Typescript interfaces, type-graphql classes then implement these. Frontend uses GraphQL API with those same Typescript interfaces. This is a bit redundant. A lot more could be invested into researching best approaches for GraphQL/Typescript combo.


### SEO
This being a game project I went with the single page app approach and intentionally did not consider using Gatsby or a similar strategy to pre-generate static HTML pages to lower load time and improve metadata for search engine optimization - something I otherwise recognize as very important for any modern web project.