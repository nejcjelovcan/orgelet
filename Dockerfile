FROM node:12.18-alpine3.12

RUN apk add --no-cache bash git

RUN mkdir -p /home/node/orgelet && chown -R node:node /home/node/orgelet
ENV NODE_ENV $NODE_ENV

WORKDIR /home/node/orgelet
USER node
COPY package*.json ./
RUN yarn install

