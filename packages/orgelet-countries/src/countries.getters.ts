import bboxPolygon from '@turf/bbox-polygon'
import center from '@turf/center'

import { Vec } from 'orgelet-core/src/primitives'
import {
  CountryDataFeature,
  CountryGeometryFeature,
} from 'orgelet-core/src/countries'

import {
  countriesLists,
  countriesData,
  countriesGeometry,
  countriesAltGeometry,
} from '.'

const checkIndex = (
  iso: string,
  index: number | number[] | undefined,
  name: string,
) => {
  if (index === undefined)
    throw new Error(`Could not find ${name} for country with iso code: ${iso}`)
}

export const getCountryData = (iso: string): CountryDataFeature => {
  const index = countriesLists.dataMap[iso]
  checkIndex(iso, index, 'data')
  return countriesData.features[index]
}

export const getCountryGeometries = (iso: string): CountryGeometryFeature[] => {
  const indexes = countriesLists.geometryMap[iso]
  checkIndex(iso, indexes, 'geometry')
  return indexes.map((i) => countriesGeometry.features[i])
}

export const getCountryAltGeometries = (
  iso: string,
): CountryGeometryFeature[] => {
  const indexes = countriesLists.altGeometryMap[iso]
  if (indexes === undefined) return []
  return indexes.map((i) => countriesAltGeometry.features[i])
}

export const getCountryLabelPosition = (iso: string): [Vec, boolean] => {
  const box = getCountryData(iso).bbox!
  const boxPolygon = bboxPolygon(box)
  const c = center(boxPolygon)
  const point = c.geometry.coordinates
  let left = false
  if (point[0] > 0) left = true
  switch (iso) {
    case 'ES': // upwards
      return [[box[2], point[1] + 4], false]
    case 'FJ': // crosses prime meridian
      return [[box[2] - 5, point[1]], true]
    case 'FR': // reunion & guiana situation
      return [[7, 47], false]
    case 'KI': // crosses prime meridian
      return [[box[0] + 22, point[1]], false]
    case 'NO':
      return [[box[0] + 12, point[1]], true]
    case 'NL':
      return [[box[2], point[1] + 19.5], false]
    case 'NZ':
      return [[box[2] - 10, point[1] - 10], true]
    case 'PT':
      return [[box[2], point[1] + 2], false]
    case 'RU':
      return [[30, 50], true]
    case 'US':
      return [[-125, 45], true]
    case 'ZA':
      return [[box[0], point[1] + 5], true]

    //   return [[box[2], point[1] + 20], false]
    default:
      return [[left ? box[0] : box[2], point[1]], left]
  }
}
