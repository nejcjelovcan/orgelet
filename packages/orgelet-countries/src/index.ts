import {
  CountriesLists,
  CountryDataFeatureCollection,
  CountryGeometryFeatureCollection,
} from 'orgelet-core/src/countries'
import { RenderProjection } from 'orgelet-core/src/projection'

import lists from './generated/world/world-countries-lists.json'
import data from './generated/world/world-countries-data.json'
import geometry from './generated/world/world-countries-geometry.json'
import altGeometry from './generated/world/world-countries-alt-geometry.json'

import worldDataJson from './generated/world/world0.json'

export const countriesLists = lists as CountriesLists
export const countriesData = data as CountryDataFeatureCollection
export const countriesGeometry = geometry as CountryGeometryFeatureCollection
export const countriesAltGeometry = altGeometry as CountryGeometryFeatureCollection
export const worldDataRenderProjection = worldDataJson.renderProjection as RenderProjection

export * from './countries.getters'
export * from './countries.geocode'
export * from 'orgelet-core/src/countries' // TODO
