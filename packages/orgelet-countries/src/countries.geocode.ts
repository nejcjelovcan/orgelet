import bboxPolygon from '@turf/bbox-polygon'
import intersect from '@turf/intersect'
import { Feature, Polygon } from 'geojson'

import { getCountryGeometries } from './countries.getters'

export const isCountryAt = (
  lonlat: number[],
  iso: string,
  delta: number = 0.3,
): boolean => {
  let polygon = bboxPolygon([
    lonlat[0] - delta,
    lonlat[1] - delta,
    lonlat[0] + delta,
    lonlat[1] + delta,
  ])
  let geometries = getCountryGeometries(iso)
  for (let i = 0; i < geometries.length; i++) {
    if (intersect(polygon, geometries[i] as Feature<Polygon>) !== null) {
      return true
    }
  }
  return false
}

// import { uniqBy } from 'ramda'
// import { Feature, Polygon } from 'geojson'
// import geojsonRbush from 'geojson-rbush'
// import { point } from '@turf/helpers'
// import booleanWithin from '@turf/boolean-within'
// import bboxPolygon from '@turf/bbox-polygon'
// import intersect from '@turf/intersect'

// import { countriesExplodedFeatureCollection } from './data'
// import { CountryFeature } from './countries.naturalearth'
// import { getPointBox } from '../primitives'

// export const countryIndex = geojsonRbush()
// countryIndex.load(countriesExplodedFeatureCollection)

// export const isCountryAt = (
//   lonlat: number[],
//   country: CountryFeature,
//   delta: number = 0.3,
// ): boolean => {
//   let polygon = bboxPolygon([
//     lonlat[0] - delta,
//     lonlat[1] - delta,
//     lonlat[0] + delta,
//     lonlat[1] + delta,
//   ])
//   console.log('POINT', lonlat)
//   console.log('POLYGON', polygon)
//   console.log('COUNTRY', country)
//   console.log('INTERSECT', intersect(polygon, country as Feature<Polygon>))
//   return intersect(polygon, country as Feature<Polygon>) !== null
// }

// export const findCountryAt = (lonlat: number[]): CountryFeature | undefined => {
//   let box = getPointBox(lonlat, [-0.3, -0.3, 0.3, 0.3])
//   let features = countryIndex.search(box).features
//   features = uniqBy((c) => c.properties.ISO_A2, features)

//   const matched = features.find((feature) =>
//     booleanWithin(point(lonlat), feature),
//   )
//   // console.log(features, matched?.properties.NAME)
//   if (features.length === 1) return features[0]
//   return matched
// }
