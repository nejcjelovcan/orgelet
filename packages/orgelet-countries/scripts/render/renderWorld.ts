import fs from 'fs'
import path from 'path'
import Jimp from 'jimp'
import { PIXI } from 'node-pixi'

import {
  getRenderProjectionFunction,
  getViewSize,
} from 'orgelet-core/src/projection'
import { earthPalette } from 'orgelet-core/src/colors'
import { getGeoJsonPolygons } from 'orgelet-core/src/geojson'
import { CountryFullFeature } from 'orgelet-core/src/countries'

import {
  drawHelperGeojson,
  geojsonFile,
  getPrerenderArgs,
  getRenderProjection,
  jimpBlackToTransparent,
  setupApp,
} from './render.helpers'
import { Graphics } from 'pixi.js'

const {
  WORLD_WIDTH,
  WORLD_DETAIL,
  DEST_DIR,
  CONFIG,
  FILENAME_POSTFIX,
} = getPrerenderArgs()
const renderProjection = getRenderProjection([-180, -62, 180, 75], WORLD_WIDTH)
const project = getRenderProjectionFunction(renderProjection)
const fullEarthRect = [-180, -90, 180, 90]

const data = { renderProjection }
const { app, mainView, container } = setupApp(renderProjection, {
  makeMask: false,
})
const oceanView = (new PIXI.Graphics() as unknown) as Graphics
oceanView.zIndex = 10
const countriesView = (new PIXI.Graphics() as unknown) as Graphics
countriesView.zIndex = 20
const overlayView = (new PIXI.Graphics() as unknown) as Graphics
overlayView.zIndex = 30

// countriesView.zIndex = 1000
container.addChild(oceanView)
container.addChild(countriesView)
container.addChild(overlayView)
const viewSize = getViewSize(renderProjection)

const bathymetryFiles = [
  'physical/ne_10m_bathymetry_L_0',
  'physical/ne_10m_bathymetry_K_200',
  'physical/ne_10m_bathymetry_J_1000',
  'physical/ne_10m_bathymetry_I_2000',
  'physical/ne_10m_bathymetry_H_3000',
  'physical/ne_10m_bathymetry_G_4000',
  'physical/ne_10m_bathymetry_F_5000',
  'physical/ne_10m_bathymetry_E_6000',
  'physical/ne_10m_bathymetry_C_8000',
  'physical/ne_10m_bathymetry_B_9000',
  'physical/ne_10m_bathymetry_A_10000',
]

const drawOcean = () => {
  oceanView.beginFill(earthPalette.waterColors[0])
  oceanView.drawRect(0, 0, viewSize[0], viewSize[1])
  oceanView.endFill()

  bathymetryFiles.forEach((name, i) => {
    drawHelperGeojson({
      name,
      graphics: oceanView,
      project,
      fill: earthPalette.waterColors[i],
    })
  })
}

const drawOverlay = () => {
  // glaciated areas
  overlayView.lineStyle(1.5, 0xffffff, 1, 0.5, false) // 1.5 fixes a thin line in greenland
  drawHelperGeojson({
    name: 'physical/ne_10m_glaciated_areas',
    graphics: overlayView,
    project,
    fill: 0xffffff,
  })

  // rivers
  const rivers = geojsonFile('physical/ne_10m_rivers_lake_centerlines').features
  rivers.forEach((feature: CountryFullFeature) => {
    getGeoJsonPolygons(feature, project).forEach((polygon) => {
      overlayView.lineStyle(
        0.8 / (feature.properties.min_zoom - 1),
        earthPalette.waterColors[0],
        1,
        1,
        false,
      )
      overlayView.beginFill(0, 0)
      // @ts-ignore
      overlayView.currentPath.shape.closed = false
      overlayView.drawPolygon(polygon)
      // @ts-ignore
      overlayView.currentPath.shape.closed = false
      overlayView.endFill()
    })
  })

  // lakes
  overlayView.lineStyle(0.5, earthPalette.waterColors[0], 1, 1, false)
  drawHelperGeojson({
    name: 'physical/ne_10m_lakes',
    graphics: overlayView,
    project,
    fill: earthPalette.waterColors[0],
  })

  // coastline
  // overlayView.lineStyle(0.5, colors.earthPalette.waterColors[0], 1, 0, false)
  overlayView.lineStyle(0.5, 0xffffff, 1, 0, false)
  drawHelperGeojson({
    name: 'physical/ne_50m_ocean',
    graphics: overlayView,
    project,
    openPath: true,
  })
  overlayView.lineStyle(1, 0xffffff, 1, 0, false)
  drawHelperGeojson({
    name: 'physical/ne_50m_coastline',
    graphics: overlayView,
    project,
    openPath: true,
  })

  // graticules
  if (CONFIG.graticules) {
    overlayView.lineStyle(0.3, 0xffffff, 1, 0.5, false)
    drawHelperGeojson({
      name: 'physical/ne_50m_graticules_10',
      graphics: overlayView,
      project,
      openPath: true,
    })
    overlayView.lineStyle(0.6, 0xffffff, 1, 0.5, false)
    drawHelperGeojson({
      name: 'physical/ne_50m_graticules_30',
      graphics: overlayView,
      project,
      openPath: true,
      featureFilter: (f) =>
        (f.properties.direction === 'S' ||
          f.properties.direction === 'N' ||
          f.properties.direction === null) &&
        f.properties.degrees === 0,
    })
  }
}

const drawCountries = (transparent?: boolean) => {
  countryList.forEach((country: CountryFullFeature) => {
    const polygons = getGeoJsonPolygons(country, project)

    if (transparent) {
      countriesView.beginFill(0, 0)
      countriesView.lineStyle(
        0.5,
        CONFIG.transparent ? 0x000000 : 0xffffff,
        1,
        0.5,
        false,
      )
    } else {
      countriesView.beginFill(CONFIG.transparent ? 0xffffff : earthPalette.gray)
      countriesView.lineStyle(
        1,
        CONFIG.transparent ? 0x000000 : earthPalette.gray,
        1,
        0.5,
        false,
      )
    }

    polygons.forEach((polygon) => countriesView.drawPolygon(polygon))
    countriesView.endFill()
  })
}

const drawWorld = (
  countryList: CountryFullFeature[],
  callback: (success: boolean) => void,
) => {
  app.renderer.resize(viewSize[0], viewSize[1])
  if (!CONFIG.transparent) {
    drawOcean()
  }
  drawCountries()
  if (CONFIG.borders) drawCountries(true)

  if (!CONFIG.transparent) {
    drawOverlay()
  }

  app.render()
  app.view
    .toBuffer('png')
    .then((buffer: Buffer) => {
      const fn = path.join(DEST_DIR, `world${FILENAME_POSTFIX}.png`)
      const imageCb = (image: Jimp) => {
        image.write(fn, () => {
          callback(true)
        })
      }
      if (CONFIG.transparent) {
        jimpBlackToTransparent(buffer, imageCb)
      } else {
        Jimp.read(buffer).then(imageCb)
      }
    })
    .catch((err: Error) => {
      console.error(err)
      callback(false)
    })
}

// const countryList = countries.collections.countries10.features
const collection = JSON.parse(
  fs
    .readFileSync(
      `natural-earth-geojson/${WORLD_DETAIL}m/cultural/ne_${WORLD_DETAIL}m_admin_0_countries.json`,
    )
    .toString(),
)
const countryList = collection.features
const main = () => {
  drawWorld(countryList, (success) => {
    fs.writeFileSync(
      path.join(DEST_DIR, `world${FILENAME_POSTFIX}.json`),
      JSON.stringify(data, null, '  '),
    )
    process.exit(success ? 0 : 1)
  })
}
main()
