import path from 'path'
import fs from 'fs'

import { Graphics } from 'pixi.js'
import { PIXI } from 'node-pixi'
import Jimp from 'jimp'

import {
  calculateNaturalRatio,
  RenderProjection,
  getViewSize,
  ProjectFunction,
} from 'orgelet-core/src/projection'

import { getGeoJsonPolygons } from 'orgelet-core/src/geojson'
import { Vec, BBox } from 'orgelet-core/src/primitives'
import { palette } from 'orgelet-core/src/colors'
import {
  CountryFullFeatureCollection,
  CountryFullFeature,
} from 'orgelet-core/src/countries'

const defaultConfig = { transparent: false, borders: false, graticules: false }
const configs = [
  defaultConfig,
  { ...defaultConfig, graticules: true },
  { ...defaultConfig, graticules: true, borders: true },
]

export const getPrerenderArgs = () => {
  if (process.argv.length < 6) {
    console.log('Usage: script.js worldWidth worldDetail destDir')
    console.log('e.g.: script.js ./prerender/countries 0 4096 50')
    process.exit(1)
  }

  const DEST_DIR = process.argv[2]
  const cIndex = parseInt(process.argv[3])
  const WORLD_WIDTH = parseInt(process.argv[4]) // width of the world
  const WORLD_DETAIL = parseInt(process.argv[5]) // 110, 50 or 10 (most detailed)
  const CONFIG = configs[cIndex]

  if (WORLD_WIDTH <= 0 || WORLD_WIDTH > 10000) {
    console.error('World width should be between 0 and 10000')
    process.exit(1)
  }
  if (WORLD_DETAIL !== 10 && WORLD_DETAIL !== 50 && WORLD_DETAIL !== 110) {
    console.error('World details should be 110, 50, or 10 (most detailed)')
    process.exit(1)
  }

  return {
    WORLD_WIDTH,
    WORLD_DETAIL,
    DEST_DIR,
    CONFIG,
    FILENAME_POSTFIX: cIndex,
  }
}

export const getRenderProjection = (
  coordinateRect: BBox,
  WORLD_WIDTH: number,
) => {
  const windowSize = {
    width: WORLD_WIDTH,
    height: WORLD_WIDTH * calculateNaturalRatio({ coordinateRect }),
  }

  return {
    coordinateRect: coordinateRect,
    viewRect: [0, 0, windowSize.width, windowSize.height],
  }
}

export const makeDrawBackground = ({
  renderProjection,
  offset,
  scale,
  color,
}: {
  renderProjection: RenderProjection
  offset?: Vec
  scale?: number
  color?: number
}) => (g: Graphics): void => {
  const viewSize = getViewSize(renderProjection)
  g.beginFill(color !== undefined ? color : palette.blue[700])
  g.drawRect(
    renderProjection.viewRect[0] + (offset ? offset[0] : 0),
    renderProjection.viewRect[1] + (offset ? offset[1] : 0),
    viewSize[0] * (scale ? scale : 1),
    viewSize[1] * (scale ? scale : 1),
  )
  g.endFill()
}

export const setupApp = (
  renderProjection: RenderProjection,
  { makeMask }: { makeMask: boolean },
) => {
  const app = new PIXI.Application(400, 400, {
    preserveDrawingBuffer: true,
    forceCanvas: true,
    backgroundColor: 0x000000,
  })
  const mainView = new PIXI.Graphics()
  mainView.zIndex = 1
  const container = new PIXI.Container()
  container.sortableChildren = true
  const mask = new PIXI.Graphics()
  let drawBackground = makeDrawBackground({
    renderProjection,
  })
  if (makeMask !== false) {
    container.mask = mask
    container.addChild(mask)
  }
  // container.addChild(mainView)
  app.stage.addChild(container)
  return { app, mainView, container, drawBackground, mask }
}

export const jimpBlackToTransparent = (
  buffer: Buffer,
  callback: (image: Jimp) => void,
) => {
  Jimp.read(buffer).then((image) => {
    // replace black with transparent
    image.scan(0, 0, image.bitmap.width, image.bitmap.height, (x, y, idx) => {
      var red = image.bitmap.data[idx + 0]

      image.bitmap.data[idx + 0] = 255 // r
      image.bitmap.data[idx + 1] = 255 // g
      image.bitmap.data[idx + 2] = 255 // b
      image.bitmap.data[idx + 3] = red // a
    })
    callback(image)
  })
}

export const drawHelper = ({
  graphics,
  features,
  project,
  fill,
  openPath,
}: {
  graphics: Graphics
  features: CountryFullFeature[]
  project: ProjectFunction
  fill?: number
  openPath?: boolean
}) => {
  features.forEach((feature) => {
    getGeoJsonPolygons(feature, project).forEach((polygon) => {
      if (fill === undefined) {
        graphics.beginFill(0, 0)
      } else {
        graphics.beginFill(fill)
      }
      // @ts-ignore
      if (openPath) graphics.currentPath.shape.closed = false
      // console.log('POLYGON', polygon)
      graphics.drawPolygon(polygon)
      graphics.endFill()
    })
  })
}

const GEOJSON_DIR = 'natural-earth-geojson/'
const GEOJSON_NAME_REG = /^(cultural|physical)\/(ne_(10|50|110)m_[^\s.]+)(\.json)?/

export const geojsonFile = (name: string) => {
  const match = GEOJSON_NAME_REG.exec(name)
  if (match) {
    return JSON.parse(
      fs
        .readFileSync(
          path.join(GEOJSON_DIR, `${match[3]}m`, match[1], `${match[2]}.json`),
        )
        .toString(),
    )
  } else {
    throw new Error(`Could not parse geojson name: ${name}`)
  }
}

export const drawHelperGeojson = ({
  name,
  graphics,
  project,
  fill,
  openPath,
  featureFilter,
}: {
  name: string
  graphics: Graphics
  project: ProjectFunction
  fill?: number
  openPath?: boolean
  featureFilter?(f: CountryFullFeature): boolean
}) => {
  drawHelper({
    graphics,
    features: geojsonFile(name).features.filter(
      featureFilter ? featureFilter : (f: CountryFullFeature) => f,
    ),
    project,
    fill,
    openPath,
  })
}
