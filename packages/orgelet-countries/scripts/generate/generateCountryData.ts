import { getRenderProjectionFunction } from 'orgelet-core/src/projection'
import { getRenderProjectionReverseFunction } from 'orgelet-core/src/projection'

import { writeFileSync, readFileSync } from 'fs'
import { join } from 'path'
import { pick } from 'ramda'

import { polygon } from '@turf/helpers'
import center from '@turf/center'
import bbox from '@turf/bbox'
import bboxPolygon from '@turf/bbox-polygon'
import truncate from '@turf/truncate'
import area from '@turf/area'
import union from '@turf/union'
import circle from '@turf/circle'
import intersect from '@turf/intersect'

import {
  CountryFullFeature,
  CountryFullFeatureCollection,
} from 'orgelet-core/src/countries'
import {
  Geometry,
  Feature,
  GeoJsonProperties,
  GeoJsonGeometryTypes,
  MultiPolygon,
  Polygon,
  GeometryCollection,
} from 'geojson'

if (process.argv.length < 3) {
  console.log('Usage: script.js destDir')
  process.exit(1)
}
const DEST_DIR = process.argv[2]

const renderProjection = JSON.parse(
  readFileSync(join(DEST_DIR, 'world0.json')).toString(),
).renderProjection
const project = getRenderProjectionFunction(renderProjection)
const projectReverse = getRenderProjectionReverseFunction(renderProjection)
const coordinateRectPolygon = bboxPolygon(renderProjection.coordinateRect)

const countriesNaturalearth = JSON.parse(
  readFileSync(
    './natural-earth-geojson/50m/cultural/ne_50m_admin_0_countries.json',
  ).toString(),
)

const countriesSovereign = {
  ...countriesNaturalearth,
  features: countriesNaturalearth.features
    .filter(
      (f: CountryFullFeature) =>
        (f.properties.SOVEREIGNT === f.properties.NAME_LONG ||
          f.properties.SOVEREIGNT === f.properties.NAME ||
          f.properties.TYPE === 'Sovereign country') &&
        f.properties.NAME !== 'Antarctica' &&
        f.properties.TYPE !== 'Indeterminate',
    )
    .map((f: CountryFullFeature) => {
      // @ts-ignore
      f.geometry = intersect(coordinateRectPolygon, f)!.geometry
      return f
    }),
}

const countriesCountryinfo = JSON.parse(
  readFileSync('./data/countryInfo.json').toString(),
)

const TRUNCATE_GEOMETRY_OPTIONS = { precision: 3 }

const NATURAL_EARTH_PROPERTIES = [
  'ADMIN',
  'NAME',
  'NAME_LONG',
  'POP_EST',
  'POP_YEAR',
  'GDP_MD_EST',
  'GDP_YEAR',
  'CONTINENT',
  'WOE_ID',
  'ISO_A2',
  'SOVEREIGNT',
]

const COUNTRY_INFO_PROPERTIES = ['emoji', 'ISO_A2']

// geometries with area smaller than this (in square km)
// will be treated as small
const ALT_SMALL_AREA_SQKM = 15000

const ALT_TARGET_MIN_DIM = 2
const ALT_TARGET_AREA_SQKM = 50000
const ALT_SCALE = 1.2
const ALT_OUTLINE_RADIUS_KM = 150
const ALT_OUTLINE_RADIUS_PX = 10
const ALT_CIRCLE_STEPS = 20

const LIST_SMALL_COUNTRY_SQKM = 15000
const LIST_SMALL_COUNTRY_MANUAL = ['FJ']

// const DEST_DIR = 'dist/countries/'

// lists of country iso codes
// maps of iso codes to data and geometry
const DEST_COUNTRIES_LISTS = 'world-countries-lists.json'

// feature collection with full properties and bounding box as geometry
const DEST_COUNTRIES_DATA = 'world-countries-data.json'

// exploded feature collection with full geometry (only ISO_A2 in properties)
const DEST_COUNTRIES_GEOMETRY = 'world-countries-geometry.json'

// feature collection of small countries with alternative outline geometry
const DEST_COUNTRIES_ALT_GEOMETRY = 'world-countries-alt-geometry.json'

const getCountryIso = (country: CountryFullFeature) => {
  let iso2 = country.properties.ISO_A2
  if (country.properties.NAME === 'France') iso2 = 'FR'
  if (country.properties.NAME === 'Norway') iso2 = 'NO'
  if (country.properties.NAME === 'Kosovo') iso2 = 'XK'
  if (country.properties.NAME === 'N. Cyprus') iso2 = 'XN'
  if (iso2 === '-99') {
    console.error(country.properties)
    throw new Error("Can't get iso for country " + country.properties.NAME)
  }
  return iso2
}

const getCountryInfo = (country: CountryFullFeature) => {
  let iso2 = getCountryIso(country)
  if (iso2 in countriesCountryinfo.code.ISO2) {
    return { ...countriesCountryinfo.code.ISO2[iso2], ISO_A2: iso2 }
  }
  // console.log('MISSING COUNTRY INFO DATA FOR', country.properties)
  return { emoji: undefined, ISO_A2: iso2 }
}

// === DATA ===
const data = {
  ...countriesSovereign,
  features: countriesSovereign.features.map((feature: CountryFullFeature) => {
    const f = truncate(
      bboxPolygon(bbox(feature), {
        properties: {
          ...pick(NATURAL_EARTH_PROPERTIES, feature.properties),
          ...pick(COUNTRY_INFO_PROPERTIES, getCountryInfo(feature)),
        },
      }),
      TRUNCATE_GEOMETRY_OPTIONS,
    )
    delete f.geometry
    return f
  }),
}

// === GEOMETRY ===

// explode multipolygons for smaller bounding boxes
// (this helps with rbush tree search and prepares stuff for highlighting small countries)

const polys = countriesSovereign.features.filter(
  (f: CountryFullFeature) => f.geometry.type === 'Polygon',
)
const mps = countriesSovereign.features.filter(
  (f: CountryFullFeature) => f.geometry.type === 'MultiPolygon',
)

const countriesSovereignExploded = {
  ...countriesSovereign,
  features: mps.reduce(
    (acc: CountryFullFeature[], mp: CountryFullFeature) =>
      mp.geometry.type === 'MultiPolygon'
        ? acc.concat(
            mp.geometry.coordinates.map((rings) =>
              polygon(rings, mp.properties),
            ),
          )
        : acc,
    polys,
  ),
}

const geometry = {
  ...countriesSovereignExploded,
  features: countriesSovereignExploded.features.map(
    (feature: CountryFullFeature) => {
      return {
        // @ts-ignore
        ...truncate(feature, TRUNCATE_GEOMETRY_OPTIONS),
        properties: { ISO_A2: getCountryIso(feature) },
      }
    },
  ),
}

// === ALT GEOMETRY ===

const altGeometry = {
  ...geometry,
  features: [],
}

countriesSovereign.features.forEach((feature: CountryFullFeature) => {
  const iso = getCountryIso(feature)
  const geometries = countriesSovereignExploded.features.filter(
    (f: CountryFullFeature) => getCountryIso(f) === iso,
  )
  const smallGeometries = geometries.filter(
    // @ts-ignore
    (g: Geometry) => area(g) < ALT_SMALL_AREA_SQKM * 1000000,
  )
  const singleGeometry = countriesSovereign.features.find(
    (f: CountryFullFeature) => getCountryIso(f) === iso,
  )

  if (smallGeometries.length === geometries.length || iso === 'FJ') {
    // union of circles around small geometries
    const outlines: Feature<Geometry, GeoJsonProperties>[] = []
    smallGeometries.forEach((g: Geometry) => {
      const cent = center(bboxPolygon(bbox(g))).geometry.coordinates
      const pCent = project(cent)
      // fixed radius in PX because geo coords vary along meridian
      const dRadiusY =
        projectReverse(pCent)[1] -
        projectReverse([pCent[0], pCent[1] + ALT_OUTLINE_RADIUS_PX])[1]

      outlines.push(
        circle(cent, dRadiusY, {
          steps: ALT_CIRCLE_STEPS,
          units: 'degrees',
        }),
      )
      // explode(g).features.forEach((feature) => {
      //   outlines.push(
      //     circle(feature.geometry.coordinates, dRadiusY, {
      //       steps: ALT_CIRCLE_STEPS,
      //       units: 'degrees',
      //     }),
      //   )
      // })
    })

    const outline =
      outlines.length < 2
        ? outlines[0]
        : outlines
            .slice(1)
            .reduce(
              (
                item: Feature<Geometry, GeoJsonProperties>,
                item2: Feature<Geometry, GeoJsonProperties>,
              ) =>
                union(
                  (item as unknown) as Feature<
                    Polygon | MultiPolygon,
                    GeoJsonProperties
                  >,
                  (item2 as unknown) as Feature<
                    Polygon | MultiPolygon,
                    GeoJsonProperties
                  >,
                ),
              outlines[0],
            )
    // console.log('OUTLINES', outlines)
    // const outline = outlines.length < 2 ? outlines[0] : union(...outlines)
    outline.properties = { ISO_A2: iso }
    altGeometry.features.push(
      outline,
      // simplify(outline, { tolerance: 0.1, highQuality: true }),
    )
    //altGeometry.features.push(truncate(outline, TRUNCATE_GEOMETRY_OPTIONS))
  }
})

// === LISTS ===
const generateMap = (collection: CountryFullFeatureCollection) => {
  const map: { [iso: string]: number[] } = {}
  collection.features.forEach((feature, i) => {
    const iso = getCountryIso(feature)

    map[iso] = map[iso] || []
    map[iso].push(i)
  })
  return map
}

const generateMapSingle = (collection: CountryFullFeatureCollection) => {
  const map: { [iso: string]: number } = {}
  collection.features.forEach((feature, i) => {
    const iso = getCountryIso(feature)

    map[iso] = i
  })
  return map
}

const lists = {
  sovereign: data.features.map((feature: CountryFullFeature) =>
    getCountryIso(feature),
  ),
  small: countriesSovereign.features
    .filter(
      (feature: CountryFullFeature) =>
        area(feature) < LIST_SMALL_COUNTRY_SQKM * 1000000 ||
        LIST_SMALL_COUNTRY_MANUAL.includes(getCountryIso(feature)),
    )
    .map((feature: CountryFullFeature) => getCountryIso(feature)),
  dataMap: generateMapSingle(data),
  geometryMap: generateMap(geometry),
  altGeometryMap: generateMap(altGeometry),
}

// === WRITE ===
writeFileSync(join(DEST_DIR, DEST_COUNTRIES_LISTS), JSON.stringify(lists))
writeFileSync(join(DEST_DIR, DEST_COUNTRIES_DATA), JSON.stringify(data))
writeFileSync(join(DEST_DIR, DEST_COUNTRIES_GEOMETRY), JSON.stringify(geometry))
writeFileSync(
  join(DEST_DIR, DEST_COUNTRIES_ALT_GEOMETRY),
  JSON.stringify(altGeometry),
)
