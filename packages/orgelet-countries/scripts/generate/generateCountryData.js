"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const projection_1 = require("orgelet-core/src/projection");
const projection_2 = require("orgelet-core/src/projection");
const fs_1 = require("fs");
const path_1 = require("path");
const ramda_1 = require("ramda");
const helpers_1 = require("@turf/helpers");
const center_1 = __importDefault(require("@turf/center"));
const bbox_1 = __importDefault(require("@turf/bbox"));
const bbox_polygon_1 = __importDefault(require("@turf/bbox-polygon"));
const truncate_1 = __importDefault(require("@turf/truncate"));
const area_1 = __importDefault(require("@turf/area"));
const union_1 = __importDefault(require("@turf/union"));
const circle_1 = __importDefault(require("@turf/circle"));
const intersect_1 = __importDefault(require("@turf/intersect"));
if (process.argv.length < 3) {
    console.log('Usage: script.js destDir');
    process.exit(1);
}
const DEST_DIR = process.argv[2];
const renderProjection = JSON.parse(fs_1.readFileSync(path_1.join(DEST_DIR, 'world0.json')).toString()).renderProjection;
const project = projection_1.getRenderProjectionFunction(renderProjection);
const projectReverse = projection_2.getRenderProjectionReverseFunction(renderProjection);
const coordinateRectPolygon = bbox_polygon_1.default(renderProjection.coordinateRect);
const countriesNaturalearth = JSON.parse(fs_1.readFileSync('./natural-earth-geojson/50m/cultural/ne_50m_admin_0_countries.json').toString());
const countriesSovereign = Object.assign(Object.assign({}, countriesNaturalearth), { features: countriesNaturalearth.features
        .filter((f) => (f.properties.SOVEREIGNT === f.properties.NAME_LONG ||
        f.properties.SOVEREIGNT === f.properties.NAME ||
        f.properties.TYPE === 'Sovereign country') &&
        f.properties.NAME !== 'Antarctica' &&
        f.properties.TYPE !== 'Indeterminate')
        .map((f) => {
        // @ts-ignore
        f.geometry = intersect_1.default(coordinateRectPolygon, f).geometry;
        return f;
    }) });
const countriesCountryinfo = JSON.parse(fs_1.readFileSync('./data/countryInfo.json').toString());
const TRUNCATE_GEOMETRY_OPTIONS = { precision: 3 };
const NATURAL_EARTH_PROPERTIES = [
    'ADMIN',
    'NAME',
    'NAME_LONG',
    'POP_EST',
    'POP_YEAR',
    'GDP_MD_EST',
    'GDP_YEAR',
    'CONTINENT',
    'WOE_ID',
    'ISO_A2',
    'SOVEREIGNT',
];
const COUNTRY_INFO_PROPERTIES = ['emoji', 'ISO_A2'];
// geometries with area smaller than this (in square km)
// will be treated as small
const ALT_SMALL_AREA_SQKM = 15000;
const ALT_TARGET_MIN_DIM = 2;
const ALT_TARGET_AREA_SQKM = 50000;
const ALT_SCALE = 1.2;
const ALT_OUTLINE_RADIUS_KM = 150;
const ALT_OUTLINE_RADIUS_PX = 10;
const ALT_CIRCLE_STEPS = 20;
const LIST_SMALL_COUNTRY_SQKM = 15000;
const LIST_SMALL_COUNTRY_MANUAL = ['FJ'];
// const DEST_DIR = 'dist/countries/'
// lists of country iso codes
// maps of iso codes to data and geometry
const DEST_COUNTRIES_LISTS = 'world-countries-lists.json';
// feature collection with full properties and bounding box as geometry
const DEST_COUNTRIES_DATA = 'world-countries-data.json';
// exploded feature collection with full geometry (only ISO_A2 in properties)
const DEST_COUNTRIES_GEOMETRY = 'world-countries-geometry.json';
// feature collection of small countries with alternative outline geometry
const DEST_COUNTRIES_ALT_GEOMETRY = 'world-countries-alt-geometry.json';
const getCountryIso = (country) => {
    let iso2 = country.properties.ISO_A2;
    if (country.properties.NAME === 'France')
        iso2 = 'FR';
    if (country.properties.NAME === 'Norway')
        iso2 = 'NO';
    if (country.properties.NAME === 'Kosovo')
        iso2 = 'XK';
    if (country.properties.NAME === 'N. Cyprus')
        iso2 = 'XN';
    if (iso2 === '-99') {
        console.error(country.properties);
        throw new Error("Can't get iso for country " + country.properties.NAME);
    }
    return iso2;
};
const getCountryInfo = (country) => {
    let iso2 = getCountryIso(country);
    if (iso2 in countriesCountryinfo.code.ISO2) {
        return Object.assign(Object.assign({}, countriesCountryinfo.code.ISO2[iso2]), { ISO_A2: iso2 });
    }
    // console.log('MISSING COUNTRY INFO DATA FOR', country.properties)
    return { emoji: undefined, ISO_A2: iso2 };
};
// === DATA ===
const data = Object.assign(Object.assign({}, countriesSovereign), { features: countriesSovereign.features.map((feature) => {
        const f = truncate_1.default(bbox_polygon_1.default(bbox_1.default(feature), {
            properties: Object.assign(Object.assign({}, ramda_1.pick(NATURAL_EARTH_PROPERTIES, feature.properties)), ramda_1.pick(COUNTRY_INFO_PROPERTIES, getCountryInfo(feature))),
        }), TRUNCATE_GEOMETRY_OPTIONS);
        delete f.geometry;
        return f;
    }) });
// === GEOMETRY ===
// explode multipolygons for smaller bounding boxes
// (this helps with rbush tree search and prepares stuff for highlighting small countries)
const polys = countriesSovereign.features.filter((f) => f.geometry.type === 'Polygon');
const mps = countriesSovereign.features.filter((f) => f.geometry.type === 'MultiPolygon');
const countriesSovereignExploded = Object.assign(Object.assign({}, countriesSovereign), { features: mps.reduce((acc, mp) => mp.geometry.type === 'MultiPolygon'
        ? acc.concat(mp.geometry.coordinates.map((rings) => helpers_1.polygon(rings, mp.properties)))
        : acc, polys) });
const geometry = Object.assign(Object.assign({}, countriesSovereignExploded), { features: countriesSovereignExploded.features.map((feature) => {
        return Object.assign(Object.assign({}, truncate_1.default(feature, TRUNCATE_GEOMETRY_OPTIONS)), { properties: { ISO_A2: getCountryIso(feature) } });
    }) });
// === ALT GEOMETRY ===
const altGeometry = Object.assign(Object.assign({}, geometry), { features: [] });
countriesSovereign.features.forEach((feature) => {
    const iso = getCountryIso(feature);
    const geometries = countriesSovereignExploded.features.filter((f) => getCountryIso(f) === iso);
    const smallGeometries = geometries.filter(
    // @ts-ignore
    (g) => area_1.default(g) < ALT_SMALL_AREA_SQKM * 1000000);
    const singleGeometry = countriesSovereign.features.find((f) => getCountryIso(f) === iso);
    if (smallGeometries.length === geometries.length || iso === 'FJ') {
        // union of circles around small geometries
        const outlines = [];
        smallGeometries.forEach((g) => {
            const cent = center_1.default(bbox_polygon_1.default(bbox_1.default(g))).geometry.coordinates;
            const pCent = project(cent);
            // fixed radius in PX because geo coords vary along meridian
            const dRadiusY = projectReverse(pCent)[1] -
                projectReverse([pCent[0], pCent[1] + ALT_OUTLINE_RADIUS_PX])[1];
            outlines.push(circle_1.default(cent, dRadiusY, {
                steps: ALT_CIRCLE_STEPS,
                units: 'degrees',
            }));
            // explode(g).features.forEach((feature) => {
            //   outlines.push(
            //     circle(feature.geometry.coordinates, dRadiusY, {
            //       steps: ALT_CIRCLE_STEPS,
            //       units: 'degrees',
            //     }),
            //   )
            // })
        });
        const outline = outlines.length < 2
            ? outlines[0]
            : outlines
                .slice(1)
                .reduce((item, item2) => union_1.default(item, item2), outlines[0]);
        // console.log('OUTLINES', outlines)
        // const outline = outlines.length < 2 ? outlines[0] : union(...outlines)
        outline.properties = { ISO_A2: iso };
        altGeometry.features.push(outline);
        //altGeometry.features.push(truncate(outline, TRUNCATE_GEOMETRY_OPTIONS))
    }
});
// === LISTS ===
const generateMap = (collection) => {
    const map = {};
    collection.features.forEach((feature, i) => {
        const iso = getCountryIso(feature);
        map[iso] = map[iso] || [];
        map[iso].push(i);
    });
    return map;
};
const generateMapSingle = (collection) => {
    const map = {};
    collection.features.forEach((feature, i) => {
        const iso = getCountryIso(feature);
        map[iso] = i;
    });
    return map;
};
const lists = {
    sovereign: data.features.map((feature) => getCountryIso(feature)),
    small: countriesSovereign.features
        .filter((feature) => area_1.default(feature) < LIST_SMALL_COUNTRY_SQKM * 1000000 ||
        LIST_SMALL_COUNTRY_MANUAL.includes(getCountryIso(feature)))
        .map((feature) => getCountryIso(feature)),
    dataMap: generateMapSingle(data),
    geometryMap: generateMap(geometry),
    altGeometryMap: generateMap(altGeometry),
};
// === WRITE ===
fs_1.writeFileSync(path_1.join(DEST_DIR, DEST_COUNTRIES_LISTS), JSON.stringify(lists));
fs_1.writeFileSync(path_1.join(DEST_DIR, DEST_COUNTRIES_DATA), JSON.stringify(data));
fs_1.writeFileSync(path_1.join(DEST_DIR, DEST_COUNTRIES_GEOMETRY), JSON.stringify(geometry));
fs_1.writeFileSync(path_1.join(DEST_DIR, DEST_COUNTRIES_ALT_GEOMETRY), JSON.stringify(altGeometry));
