import { useState, useEffect } from 'react'

type Size = { width: number; height: number }

const useWindowSize = (): Size | undefined => {
  const isClient = typeof window === 'object'

  function getSize() {
    return isClient
      ? {
          width: window.innerWidth,
          height: window.innerHeight,
        }
      : undefined
  }

  const [windowSize, setWindowSize] = useState(getSize)

  useEffect(() => {
    if (!isClient) {
      return
    }

    function handleResize() {
      setWindowSize(getSize())
    }

    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []) // Empty array ensures that effect is only run on mount and unmount

  return windowSize
}

export default useWindowSize
