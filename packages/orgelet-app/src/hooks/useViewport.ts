import { useState, useEffect } from 'react'
import { fitViewport } from '../view/core'

const useViewport = ({
  size,
  contentSize,
}: {
  size: number[]
  contentSize: number[]
}) => {
  const [offsetAndScale, setOffsetAndScale] = useState({
    offset: [0, 0],
    scale: 1,
  })

  useEffect(() => {
    setOffsetAndScale(fitViewport({ size, contentSize }))
  }, [size, contentSize])

  return offsetAndScale
}

export default useViewport
