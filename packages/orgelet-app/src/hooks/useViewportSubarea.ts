import { useState, useEffect } from 'react'

import { BBox, getBBoxSize } from 'orgelet-core/src/primitives'
import { fitViewport } from '../view/core'

const useViewportSubarea = ({
  size,
  subarea,
}: {
  size: number[]
  subarea: BBox
}) => {
  const [offsetAndScale, setOffsetAndScale] = useState({
    offset: [0, 0],
    scale: 1,
  })

  useEffect(() => {
    const { offset, scale } = fitViewport({
      size,
      contentSize: getBBoxSize(subarea),
    })
    setOffsetAndScale({
      offset: [offset[0] - subarea[0] * scale, offset[1] - subarea[1] * scale],
      scale,
    })
  }, [size, subarea])

  return offsetAndScale
}

export default useViewportSubarea
