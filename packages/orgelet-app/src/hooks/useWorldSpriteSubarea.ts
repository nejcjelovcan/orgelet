import { useMemo } from 'react'

import { BBox, Vec } from 'orgelet-core/src/primitives'
import {
  getRenderProjectionFunction,
  getRenderProjectionReverseFunction,
  projectRect,
} from 'orgelet-core/src/projection'

import { worldDataRenderProjection } from 'orgelet-countries'

import useViewportSubarea from './useViewportSubarea'

const useWorldSpriteSubarea = ({
  size,
  subarea,
}: {
  size: Vec
  subarea?: BBox
}) => {
  const project = useMemo(
    () => getRenderProjectionFunction(worldDataRenderProjection),
    [],
  )
  const reverseProject = useMemo(
    () => getRenderProjectionReverseFunction(worldDataRenderProjection),
    [],
  )
  const subareaRect = useMemo(
    () =>
      projectRect(
        project,
        subarea === undefined
          ? worldDataRenderProjection.coordinateRect
          : subarea,
      ),
    [subarea, project],
  )

  const viewport = useViewportSubarea({ size, subarea: subareaRect })

  const getCoordsFromGlobalPoint = (globalPoint: { x: number; y: number }) => {
    return reverseProject([
      (globalPoint.x - viewport.offset[0]) / viewport.scale,
      (globalPoint.y - viewport.offset[1]) / viewport.scale,
    ])
  }

  const fitSize = [
    size[0] - viewport.offset[0] * 2,
    size[1] - viewport.offset[1] * 2,
  ]

  return {
    ...viewport,
    fitSize,
    project,
    reverseProject,
    getCoordsFromGlobalPoint,
  }
}

export default useWorldSpriteSubarea
