import { useState } from 'react'
import { usePinch } from 'react-use-gesture'

const useInteractiveMap = ({
  zoomMultiplier,
  zoomRange,
  offset,
}: {
  zoomMultiplier?: number
  zoomRange?: number[]
  offset: number[]
}) => {
  const [pinchData, setPinchData] = useState({ offset: [0, 0], scale: 1 })
  if (!zoomRange) zoomRange = [1, 2]
  if (!zoomMultiplier) zoomMultiplier = 0.1

  const pinch = usePinch(
    (e) => {
      if (e.event) {
        e.event.preventDefault()
      }
      // console.log('PINCH', e.da, e.vdva, e)
      if (e.vdva[0] != 0) {
        let vscale = e.vdva[0] * zoomMultiplier!
        let nscale = pinchData.scale * (vscale + 1)
        vscale = nscale / pinchData.scale - 1

        let noffset = pinchData.offset

        if (nscale < zoomRange![0]) {
          nscale = zoomRange![0]
          vscale = nscale / pinchData.scale - 1
        } else if (nscale > zoomRange![1]) {
          nscale = zoomRange![1]
          vscale = nscale / pinchData.scale - 1
        }
        if (e.origin) {
          const origin = [e.origin[0] - offset[0], e.origin[1] - offset[1]]
          // origin[0] = origin[0]
          // origin[1] = origin[1]
          noffset = [
            pinchData.offset[0] - (pinchData.scale * vscale * origin[0]) / 1,
            pinchData.offset[1] - (pinchData.scale * vscale * origin[1]) / 1,
          ]
          if (nscale === 1) {
            noffset = [0, 0]
          }
        }
        setPinchData({ scale: nscale, offset: noffset })
      }

      return true
    },
    { eventOptions: { passive: false }, domTarget: document },
  )
  pinch()
  return pinchData
}
export default useInteractiveMap
