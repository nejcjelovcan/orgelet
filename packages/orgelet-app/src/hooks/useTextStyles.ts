import { useMemo } from 'react'
import { FontMap, TextStyleMap, makeTextStyle } from 'orgelet-core/src/fonts'

const useTextStyles = (fonts: FontMap): TextStyleMap => {
  return useMemo(() => {
    const f: TextStyleMap = {}
    Object.keys(fonts).forEach((name) => (f[name] = makeTextStyle(fonts[name])))
    return f
  }, [fonts])
}
export default useTextStyles
