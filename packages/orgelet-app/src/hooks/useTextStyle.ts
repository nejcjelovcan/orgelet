import { useMemo } from 'react'
import { Font, makeTextStyle } from 'orgelet-core/src/fonts'

const useTextStyle = (font: Partial<Font>) => {
  return useMemo(() => makeTextStyle(font), [font])
}
export default useTextStyle
