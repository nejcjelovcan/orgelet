import useViewport from './useViewport'

import { renderHook } from '@testing-library/react-hooks'

describe('useViewport', () => {
  it('returns scale and offset for the content fit', () => {
    const { result } = renderHook((props) => useViewport(props), {
      initialProps: { size: [600, 400], contentSize: [1200, 800] },
    })
    expect(result.current).toMatchObject({ scale: 0.5, offset: [0, 0] })
  })
  it('returns scale and offset for the content fit (portrait)', () => {
    const { result } = renderHook((props) => useViewport(props), {
      initialProps: { size: [400, 800], contentSize: [1000, 800] },
    })
    expect(result.current).toMatchObject({ scale: 0.4, offset: [0, 240] })
  })
  it('returns scale and offset for the content fit (landscape)', () => {
    const { result } = renderHook((props) => useViewport(props), {
      initialProps: { size: [800, 400], contentSize: [1000, 800] },
    })
    expect(result.current).toMatchObject({ scale: 0.5, offset: [150, 0] })
  })
})
