import React from 'react'
import { Stage } from '@inlet/react-pixi'

import './App.css'

import useWindowSize from './hooks/useWindowSize'

import RootView from './view/RootView'
import configureStore from './redux/store'
import { Provider } from 'react-redux'

const store = configureStore()

const App = () => {
  const windowSize = useWindowSize()
  if (!windowSize) return null
  const resolution = 2

  return (
    <div>
      <Stage
        width={windowSize.width}
        height={windowSize.height}
        options={{ antialias: true, resolution, autoDensity: true }}
      >
        <Provider store={store}>
          <RootView size={[windowSize.width, windowSize.height]} />
        </Provider>
      </Stage>
    </div>
  )
}

export default App
