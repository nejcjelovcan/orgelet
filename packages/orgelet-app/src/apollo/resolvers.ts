import { ApolloCache } from 'apollo-cache'
import { Resolvers, gql } from 'apollo-boost'

export const typeDefs = gql`
  extend type Query {
    # isLoggedIn: Boolean!
    # cartItems: [ID!]!
  }

  extend type Mutation {
    setMainState(mainState: MainState!): MainState!
  }
`

type ResolverFn = (
  parent: any,
  args: any,
  { cache }: { cache: ApolloCache<any> },
) => any

interface ResolverMap {
  [field: string]: ResolverFn
}

interface AppResolvers extends Resolvers {
  // We will update this with our app's resolvers later
}

export const resolvers = {}
