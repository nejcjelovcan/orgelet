// import ApolloClient from 'apollo-boost'

// const { REACT_APP_APOLLO_URI } = process.env

// const client = new ApolloClient({
//   uri: REACT_APP_APOLLO_URI,
// })
// export default client

import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  NormalizedCacheObject,
} from 'apollo-boost'

import { resolvers, typeDefs } from './resolvers'

const { REACT_APP_APOLLO_URI } = process.env

const cache = new InMemoryCache()
const link = new HttpLink({
  uri: REACT_APP_APOLLO_URI,
  headers: {
    authorization: localStorage.getItem('token'),
  },
})

// const client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
//   cache,
//   link,
// })
// export default client

const client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  cache,
  link,
  typeDefs,
  resolvers,
})

cache.writeData({
  data: {
    isLoggedIn: !!localStorage.getItem('token'),
    cartItems: [],
  },
})

export default client
