// import { mainLoopActionIds } from './mainLoop.actions'
// import { gameLoopActionIds } from './gameLoop.actions'

// export const actionIds = {
//   ...mainLoopActionIds,
//   ...gameLoopActionIds,
// }

export interface BaseAction {
  type: string
  payload?: any
}
