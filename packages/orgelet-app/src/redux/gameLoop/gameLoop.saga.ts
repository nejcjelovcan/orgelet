import { put, take, select } from 'redux-saga/effects'
import { gameQuestion, gameLoopActionIds, gameEnd } from './gameLoop.actions'
import { RootState } from '../root.reducer'

import { Game, getNextGameQuestion } from 'orgelet-core/src/game'
import { countriesLists } from 'orgelet-countries'

const getGame = (state: RootState): Game => state.game

function* gameLoopSaga() {
  while (true) {
    const game: Game = yield select(getGame)
    if (game.targets.length === game.length) {
      yield put(gameEnd())
      break
    }

    yield put(
      gameQuestion({ target: getNextGameQuestion(countriesLists, game) }),
    )
    yield take(gameLoopActionIds.GAME_ANSWER)
    yield take(gameLoopActionIds.GAME_NEXT)
  }
}

export default gameLoopSaga
