import { GameDefaults, Game } from 'orgelet-core/src/game'

import { BaseAction } from '../actions'
import {
  gameLoopActionIds,
  GameStartAction,
  GameQuestionAction,
  GameAnswerAction,
} from './gameLoop.actions'

export default (state = GameDefaults, action: BaseAction): Game => {
  switch (action.type) {
    case gameLoopActionIds.GAME_START:
      let startAction = action as GameStartAction
      return {
        ...state,
        ...startAction.payload,
        state: 'idle',
        targets: [],
        answers: [],
        points: [],
      }

    case gameLoopActionIds.GAME_QUESTION:
      let questionAction = action as GameQuestionAction
      return {
        ...state,
        targets: [...state.targets, questionAction.payload.target],
        state: 'question',
      }

    case gameLoopActionIds.GAME_ANSWER:
      let answerAction = action as GameAnswerAction
      let answer = answerAction.payload.answer
      return {
        ...state,
        answers: [...state.answers, answer],
        points: [
          ...state.points,
          answer === state.targets[state.targets.length - 1] ? 1 : 0,
        ],
        state: 'answer',
      }

    case gameLoopActionIds.GAME_NEXT:
      return {
        ...state,
        state: 'next',
      }

    default:
      return state
  }
}
