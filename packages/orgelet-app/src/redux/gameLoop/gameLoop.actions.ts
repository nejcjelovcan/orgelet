import { GameType, GameArea, GameOrder } from 'orgelet-core/src/game'

import { BaseAction } from '../actions'

export const gameLoopActionIds = {
  GAME_START: 'Game is started',
  GAME_QUESTION: 'Game question is stated',
  GAME_ANSWER: 'Game answer is stated',
  GAME_NEXT: 'Next question is requested',
  GAME_END: 'Game is ended',
}

export interface GameStartPayload {
  type: GameType
  area: GameArea
  order: GameOrder
  length: number
}

export interface GameStartAction extends BaseAction {
  type: typeof gameLoopActionIds.GAME_START
  payload: GameStartPayload
}

export interface GameQuestionPayload {
  target: string
}

export interface GameQuestionAction extends BaseAction {
  type: typeof gameLoopActionIds.GAME_QUESTION
  payload: GameQuestionPayload
}

export interface GameAnswerPayload {
  answer: string
}

export interface GameAnswerAction extends BaseAction {
  type: typeof gameLoopActionIds.GAME_ANSWER
  payload: GameAnswerPayload
}

export const gameStart = ({
  type,
  area,
  order,
  length,
}: GameStartPayload): GameStartAction => ({
  type: gameLoopActionIds.GAME_START,
  payload: { type, area, order, length },
})

export const gameQuestion = ({
  target,
}: GameQuestionPayload): GameQuestionAction => ({
  type: gameLoopActionIds.GAME_QUESTION,
  payload: { target },
})

export const gameAnswer = ({
  answer,
}: GameAnswerPayload): GameAnswerAction => ({
  type: gameLoopActionIds.GAME_ANSWER,
  payload: { answer },
})

export const gameEnd = () => ({ type: gameLoopActionIds.GAME_END })

export const gameNext = () => ({ type: gameLoopActionIds.GAME_NEXT })
