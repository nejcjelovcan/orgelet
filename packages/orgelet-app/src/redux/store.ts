import { applyMiddleware, compose, createStore, StoreEnhancer } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './root.reducer'
import rootSaga from './root.saga'

export const sagaMiddleware = createSagaMiddleware()

export default function configureStore() {
  const middlewares = [sagaMiddleware]
  const middlewareEnhancer = applyMiddleware(...middlewares)

  const enhancers = [middlewareEnhancer]
  const composedEnhancers = compose(...enhancers)

  const store = createStore(rootReducer, composedEnhancers as StoreEnhancer)

  sagaMiddleware.run(rootSaga)
  // const store = createStore(rootReducer)

  // if (process.env.NODE_ENV !== 'production' && module.hot) {
  //   module.hot.accept('./reducers', () => store.replaceReducer(rootReducer))
  // }

  return store
}
