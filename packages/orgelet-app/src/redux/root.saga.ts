import { all } from 'redux-saga/effects'

import { mainLoopSaga } from './mainLoop'

export default function* rootSaga() {
  yield all([mainLoopSaga()])
}
