import { BaseAction } from '../actions'
import { MainStateDefaults, MainState } from '.'
import { mainLoopActionIds } from './mainLoop.actions'
import { gameLoopActionIds } from '../gameLoop'

export default (state = MainStateDefaults, action: BaseAction): MainState => {
  switch (action.type) {
    case mainLoopActionIds.MAIN_LOADED:
      return { ...state, loaded: true, state: 'menu' }
    case mainLoopActionIds.MAIN_NEW:
      return { ...state, state: 'newGameMenu' }
    case mainLoopActionIds.MAIN_EXIT:
      return { ...state, state: 'menu' }
    case gameLoopActionIds.GAME_START:
      return { ...state, state: 'game' }
    case gameLoopActionIds.GAME_END:
      return { ...state, state: 'menu' }
    default:
      return state
  }
}
