export {
  mainLoaded,
  mainNew,
  mainExit,
  mainLoopActionIds,
} from './mainLoop.actions'
export { default as mainLoopReducer } from './mainLoop.reducer'
export { default as mainLoopSaga } from './mainLoop.saga'

export type MainStateState = 'menu' | 'newGameMenu' | 'game'

export type MainState = {
  loaded: boolean
  state: MainStateState
}

export const MainStateDefaults: MainState = {
  loaded: false,
  state: 'menu',
}
