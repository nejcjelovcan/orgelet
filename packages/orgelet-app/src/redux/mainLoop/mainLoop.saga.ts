import { take } from 'redux-saga/effects'
import { mainLoopActionIds } from './mainLoop.actions'
import { gameLoopActionIds, gameLoopSaga } from '../gameLoop'

export default function* mainLoopSaga() {
  yield take(mainLoopActionIds.MAIN_LOADED)

  while (true) {
    yield take(mainLoopActionIds.MAIN_NEW)
    let action = yield take([
      gameLoopActionIds.GAME_START,
      mainLoopActionIds.MAIN_EXIT,
    ])
    if (action.type === gameLoopActionIds.GAME_START) {
      yield gameLoopSaga()
    }
  }
}
