export const mainLoopActionIds = {
  MAIN_LOADED: 'Main stage is loaded',
  MAIN_NEW: 'New game is requested',
  MAIN_EXIT: 'Exit is requested',
}

export const mainLoaded = () => ({ type: mainLoopActionIds.MAIN_LOADED })
export const mainNew = () => ({ type: mainLoopActionIds.MAIN_NEW })
export const mainExit = () => ({ type: mainLoopActionIds.MAIN_EXIT })
