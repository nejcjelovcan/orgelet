import { combineReducers } from 'redux'
import { Game } from 'orgelet-core/src/game'

import { mainLoopReducer, MainState } from './mainLoop'
import { gameLoopReducer } from './gameLoop'

export type RootState = {
  main: MainState
  game: Game
}

export default combineReducers({
  main: mainLoopReducer,
  game: gameLoopReducer,
})
