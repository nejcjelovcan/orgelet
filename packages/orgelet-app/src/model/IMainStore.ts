export enum MainState {
  Menu = 'menu',
  MenuNewGame = 'menuNewGame',
  Game = 'game',
}

export default interface IMainStore {
  state: MainState
}
