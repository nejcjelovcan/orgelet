import React, { useEffect } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { AdjustmentFilter } from '@pixi/filter-adjustment'
import * as PIXI from 'pixi.js'
import { useApp, Container, withFilters } from '@inlet/react-pixi'

import { RootState } from '../redux/root.reducer'
import { mainLoaded } from '../redux/mainLoop'
import WorldView from './WorldView'
import WorldOverlay from './WorldOverlay'
import GameOverlay from './GameOverlay'

const connector = connect((state: RootState) => state.main, { mainLoaded })
type RootViewProps = ConnectedProps<typeof connector> & { size: number[] }

const Filters = withFilters(Container, {
  adjust: AdjustmentFilter,
  blur: PIXI.filters.BlurFilter,
})

const RootView = ({ size, mainLoaded, loaded, state }: RootViewProps) => {
  const app = useApp()
  const url = '/assets/sprites/world2.png' // TODO get path from prerender/data.ts
  useEffect(() => {
    app.loader.reset() // TODO
    app.loader.add(url).load((_, resource) => {
      mainLoaded()
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  if (!loaded) return <></>

  return (
    <Container>
      {state !== 'game' && (
        <Filters adjust={{ brightness: 0.5 }} blur={{ blur: 4 }}>
          <WorldView size={size} />
        </Filters>
      )}
      {state === 'game' && (
        <>
          <WorldView size={size} />
          <WorldOverlay size={size} />
        </>
      )}
      <GameOverlay size={size} />
    </Container>
  )
}

export default connector(RootView)
