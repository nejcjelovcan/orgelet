import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { Container } from '@inlet/react-pixi'

import { countriesLists } from 'orgelet-countries'

import { RootState } from '../redux/root.reducer'

import Menu, { MenuItem } from './components/atoms/Menu'
import { mainExit, mainNew } from '../redux/mainLoop'
import { gameStart } from '../redux/gameLoop'
import { buttonStyle } from './styles'

const connector = connect((state: RootState) => state.main, {
  mainExit,
  mainNew,
  gameStart,
})
type GameOverlayProps = ConnectedProps<typeof connector> & { size: number[] }

const GameOverlay = ({
  size,
  state,
  mainExit,
  mainNew,
  gameStart,
}: GameOverlayProps) => {
  let menuItems: MenuItem[] = []
  switch (state) {
    case 'menu':
      menuItems = [
        {
          icon: '\uf0ac',
          text: 'New Game',
          callback: () => mainNew(),
        },
        { icon: '\uf085', text: 'Options' },
        { icon: '\uf00d', text: 'Exit' },
      ]
      break
    case 'newGameMenu':
      menuItems = [
        {
          text: 'Countries',
          callback: () =>
            gameStart({
              type: 'countries',
              area: 'world',
              order: 'random',
              length: 10,
              // order: 'asc',
              // length: countriesLists.sovereign.length,
            }),
        },
        {
          text: 'Small Countries',
          callback: () =>
            gameStart({
              type: 'small',
              area: 'world',
              // order: 'random',
              // length: 10,
              order: 'asc',
              length: countriesLists.small.length,
            }),
        },
        {
          icon: '\uf104',
          text: 'Back',
          callback: () => mainExit(),
        },
      ]
      break
  }
  const spacing = 20
  const buttonSize = [200, 40]
  const pivot: [number, number] = [
    -size[0] / 2,
    -(size[1] - (buttonSize[1] + spacing) * menuItems.length) / 2,
  ]

  return (
    <Container>
      <Menu
        menuId={state}
        pivot={pivot}
        buttonBoxAnchor={[0.5, 0.5]}
        textAlign={[0, 0.5]}
        menuItems={menuItems}
        spacing={spacing}
        buttonSize={buttonSize}
        {...buttonStyle}
      />
    </Container>
  )
}

export default connector(GameOverlay)
