import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { Container } from '@inlet/react-pixi'

import { BBox, Vec } from 'orgelet-core/src/primitives'
import { earthPalette } from 'orgelet-core/src/colors'
import { isCountryAt } from 'orgelet-countries'

import useWorldSpriteSubarea from '../hooks/useWorldSpriteSubarea'

import { RootState } from '../redux/root.reducer'
import { gameAnswer } from '../redux/gameLoop'

import WorldBitmapSprite from './components/WorldBitmapSprite'
import CountryHighlight from './components/country/CountryHighlight'
import { withTwoWayTransition } from './components/atoms/Transition'

const connector = connect((state: RootState) => ({ game: state.game }), {
  gameAnswer,
})

const TransitionCountryHighlight = withTwoWayTransition(CountryHighlight)

type WorldViewProps = ConnectedProps<typeof connector> & {
  size: Vec
  subarea?: BBox
}

const WorldView = ({
  size,
  subarea,
  game: { state, answers, targets },
  gameAnswer,
}: WorldViewProps) => {
  const {
    offset,
    scale,
    project,
    getCoordsFromGlobalPoint,
  } = useWorldSpriteSubarea({
    size,
    subarea,
  })
  const iso = targets[targets.length - 1]

  const onClick = (event: PIXI.interaction.InteractionEvent) => {
    if (state === 'question') {
      const lonlat = getCoordsFromGlobalPoint(event.data.global)
      if (isCountryAt(lonlat, iso, 1)) {
        gameAnswer({ answer: iso })
      } else {
        gameAnswer({ answer: '' })
      }
    }
  }

  return (
    <Container
      position={[offset[0], offset[1]]}
      scale={scale}
      interactive
      pointerdown={onClick}
    >
      <WorldBitmapSprite url={'/world/world2.png'} />
      <TransitionCountryHighlight
        id={state === 'answer' ? iso : undefined}
        duration={20}
        properties={{ alpha: [0, 1] }}
        iso={iso}
        project={project}
        fillColor={
          answers[answers.length - 1] === targets[targets.length - 1]
            ? earthPalette.green
            : earthPalette.red
        }
      />
    </Container>
  )
}

export default connector(WorldView)
