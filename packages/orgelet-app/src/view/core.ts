import { Vec } from 'orgelet-core/src/primitives'
import { Font } from 'orgelet-core/src/fonts'

import { DisplayObject } from 'pixi.js'
import { _ReactPixi } from '@inlet/react-pixi'

export type LineType = 'normal' | 'native'

export type LineOptions = {
  lineWidth: number
  lineType: LineType
  lineColor: number
  lineAlpha: number
  linePosition: number
}

export type FillOptions = {
  fillColor: number
  fillAlpha: number
}

export type BoxOptions = {
  boxAnchor: Vec
  boxPadding: Vec
  boxSize: Vec
  boxRadius: number
}

// (select ones that are intended to be propagated)
type WithPoint<T extends keyof any> = { [P in T]: [number, number] }
type P = 'anchor' | 'position' | 'scale' | 'pivot' | 'skew'
export type IDisplayObject = Pick<
  DisplayObject,
  'alpha' | 'interactive' | 'scale' | 'rotation' | 'visible' | 'zIndex'
> &
  WithPoint<P> &
  _ReactPixi.InteractionEvents

export type DisplayObjectOptions = Partial<IDisplayObject>

export type GraphicsOptions = Partial<LineOptions> & Partial<FillOptions>

export type GraphicsStyleOptions = GraphicsOptions &
  Partial<BoxOptions> &
  Partial<Font>

export type GraphicsStyleStates = {
  over?: GraphicsStyleOptions
  down?: GraphicsStyleOptions
}

export const fitViewport = ({
  size,
  contentSize,
}: {
  size: Vec
  contentSize: Vec
}) => {
  const sizeRatio = size[1] / size[0]
  const contentRatio = contentSize[1] / contentSize[0]

  if (contentRatio > sizeRatio) {
    return {
      offset: [(size[1] / sizeRatio - size[1] / contentRatio) / 2, 0],
      scale: size[1] / contentSize[1],
    }
  } else {
  }
  return {
    offset: [0, (size[0] * sizeRatio - size[0] * contentRatio) / 2],
    scale: size[0] / contentSize[0],
  }
}
