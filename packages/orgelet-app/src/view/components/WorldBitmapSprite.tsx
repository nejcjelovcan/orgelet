import React, { ReactNode } from 'react'
import { Sprite } from '@inlet/react-pixi'

const WorldBitmapSprite = ({
  children,
  url,
}: {
  children?: ReactNode
  url?: string
}) => {
  return (
    <>
      <Sprite image={url || '/world/world2.png'} />
      {children}
    </>
  )
}

export default WorldBitmapSprite
