import React from 'react'
import { Container } from '@inlet/react-pixi'

import { Vec } from 'orgelet-core/src/primitives'

import Box from '../Box'
import TextFlow, { Text } from '../TextFlow'
import Button from '../Button'
import { buttonStyle } from '../../../styles'

const ANCHORS = [0, 0.5, 1]
const ANCHOR_PERMUTATIONS: Vec[] = ANCHORS.map((anchor1) =>
  ANCHORS.map((anchor2) => [anchor1, anchor2] as Vec),
).reduce((list, b) => list.concat(b), [])

const ButtonTest = ({ size }: { size: Vec }) => {
  return (
    <Container>
      {ANCHOR_PERMUTATIONS.map((anchor, i) => (
        <Button
          key={i}
          boxAnchor={anchor}
          position={[size[0] * anchor[0], size[1] * anchor[1]]}
          {...buttonStyle}
          boxSize={[150, 40]}
        >
          <Text font="icon">{'\uf085'}</Text> Option
        </Button>
      ))}
    </Container>
  )
}
export default ButtonTest
