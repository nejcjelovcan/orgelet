import React from 'react'
import { Container } from '@inlet/react-pixi'

import { Vec } from 'orgelet-core/src/primitives'

import Box from '../Box'
import TextFlow, { Text } from '../TextFlow'
import { infoStyle } from '../../../styles'

const ANCHORS = [0, 0.5, 1]
const ANCHOR_PERMUTATIONS: Vec[] = ANCHORS.map((anchor1) =>
  ANCHORS.map((anchor2) => [anchor1, anchor2] as Vec),
).reduce((list, b) => list.concat(b), [])

const BoxTest = ({ size }: { size: Vec }) => {
  return (
    <Container>
      {ANCHOR_PERMUTATIONS.map((anchor, i) => (
        <Box
          boxAnchor={[anchor[0], anchor[1]]}
          position={[size[0] * anchor[0], size[1] * anchor[1]]}
          boxPadding={[10, 5]}
          boxRadius={5}
          boxSize={[300, 100]}
          {...infoStyle}
        >
          <TextFlow
            font="info"
            fontFill={0xffffff}
            align={[anchor[0], anchor[1]]}
            fonts={{
              icon: { font: 'icon', fontFill: 0xffffff },
              bold: { font: 'infoBold', fontFill: 0xffffff },
            }}
          >
            <Text font="icon" width={80} align={[1, 0.5]}>
              {'\uf085'}
            </Text>{' '}
            Lorem ipsum <Text font="bold">dolor</Text> sit amet
          </TextFlow>
        </Box>
      ))}
    </Container>
  )
}
export default BoxTest
