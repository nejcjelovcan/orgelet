import React, { ReactElement, useMemo, useState } from 'react'
import { Container, Graphics } from '@inlet/react-pixi'
import { Graphics as PixiGraphics } from 'pixi.js'

import { Vec } from 'orgelet-core/src/primitives'
import {
  DisplayObjectOptions,
  GraphicsStyleOptions,
  BoxOptions,
} from '../../../view/core'

import { TextFlowProps } from './TextFlow'

export type BoxTextContextProps = {
  offset?: Vec
  size?: Vec
  textSize?: Vec
  setTextSize?: (size: Vec) => void
}

export const BoxTextContext = React.createContext<BoxTextContextProps>({})

export type BoxDrawFunctionProps = {
  actualSize: number[]
  anchorOffset: number[]
} & Pick<BoxOptions, 'boxAnchor' | 'boxPadding' | 'boxRadius'>
export type BoxDrawFunction = (
  g: PixiGraphics,
  props: BoxDrawFunctionProps,
) => void

export type BoxProps = {
  draw?: BoxDrawFunction
  children?: ReactElement<TextFlowProps>
} & DisplayObjectOptions &
  GraphicsStyleOptions

const Box = ({
  position = [0, 0],

  boxAnchor = [0, 0],
  boxPadding = [0, 0],
  boxSize: size,
  boxRadius = 0,

  fillColor = 0,
  fillAlpha = 1,

  lineColor = 0,
  lineAlpha = 1,
  linePosition = 0.5,
  lineType = 'normal',
  lineWidth = 1,

  draw,
  children,

  ...rest
}: BoxProps) => {
  const [textSize, setTextSize] = useState<Vec>([0, 0])
  const boxSize: Vec = useMemo(() => {
    if (!size && !children) {
      throw new Error('Box needs either a size property or a TextFlow child')
    }
    return size ? size : textSize ? textSize : [0, 0]
  }, [size, textSize, children])

  const expandedPadding =
    boxPadding.length > 3 ? boxPadding : boxPadding.concat(boxPadding)

  const actualSize = [
    boxSize[0] + expandedPadding[0] + expandedPadding[2],
    boxSize[1] + expandedPadding[1] + expandedPadding[3],
  ]

  const anchorOffset = [
    -boxAnchor[0] * actualSize[0],
    -boxAnchor[1] * actualSize[1],
  ]

  const childElement = children && (
    <BoxTextContext.Provider
      value={{
        offset: [
          expandedPadding[0] + (position[0] - actualSize[0] * boxAnchor[0]), //+
          expandedPadding[1] + (position[1] - actualSize[1] * boxAnchor[1]), //+
        ],
        size,
        textSize,
        setTextSize,
      }}
    >
      {children}
    </BoxTextContext.Provider>
  )
  // const visible = boxSize[0] > 0 && boxSize[1] > 0
  const visible = !children || (textSize && textSize[0] > 0)

  return (
    <Container visible={visible} {...rest}>
      <Graphics
        position={position}
        draw={(g) => {
          g.clear()
          g.beginFill(fillColor, fillAlpha)
          g.lineStyle(
            lineWidth,
            lineColor,
            lineAlpha,
            linePosition,
            lineType === 'native',
          )
          if (boxRadius > 0) {
            g.drawRoundedRect(
              anchorOffset[0],
              anchorOffset[1],
              actualSize[0],
              actualSize[1],
              boxRadius,
            )
          } else {
            g.drawRect(
              anchorOffset[0],
              anchorOffset[1],
              actualSize[0],
              actualSize[1],
            )
          }
          g.endFill()
          if (draw)
            draw(g, {
              boxAnchor,
              boxPadding,
              boxRadius,
              actualSize,
              anchorOffset,
            })
        }}
      />
      {childElement}
    </Container>
  )
}

export default Box
