import React from 'react'
import { Container } from '@inlet/react-pixi'

import { Vec } from 'orgelet-core/src/primitives'
import { GraphicsStyleStates, DisplayObjectOptions } from '../../core'

import { buttonStyle } from '../../styles'
import Button, { ButtonProps } from './Button'
import { Text } from './TextFlow'
// import BoxAnimation from './BoxAnimation'
import { useTransitionProps } from './Transition'

// const transitionDefaults: WithTwoWayTransitionInnerProps<DisplayObjectOptions> = {
//   duration: 20,
//   properties: { alpha: [0, 1] },
// }
// const TransitionBox = withTwoWayTransition(Box)
// const TransitionButton = withTwoWayTransition(Button)

// const TransitionButton = withTransition(Button, {
//   property: { name: 'alpha', start: 0, end: 1, duration: 20 },
// })

export type MenuItem = {
  text: string
  icon?: string
  callback?: () => void
}

export type MenuOptions = {
  menuItems: MenuItem[]
  buttonSize: Vec
  buttonBoxAnchor: Vec
  menuId: string
  spacing?: number
  pivot?: [number, number]

  // offset?: Vec
  // anchor?: Vec
  textAlign?: Vec
  // pivot?: Vec
} & GraphicsStyleStates &
  DisplayObjectOptions

const Menu = ({
  menuItems,
  buttonSize,
  buttonBoxAnchor,
  menuId,
  spacing = 10,
  textAlign = [0, 0.5],
  ...rest
}: MenuOptions) => {
  const [props] = useTransitionProps<ButtonProps>({
    duration: 20,
    id: menuId,
    alpha: [0, 1],
    position: [
      [0, -30],
      [0, 0],
    ],
  })

  return (
    <Container position={props.position} {...rest}>
      {menuItems.map((menuItem, i) => (
        <Button
          key={`${i}${menuItem.text}`}
          // transitionId={i}
          boxAnchor={buttonBoxAnchor}
          boxSize={buttonSize}
          align={textAlign}
          position={[0, (buttonSize[1] + spacing) * i]}
          callback={menuItem.callback}
          {...buttonStyle}
          alpha={props.alpha}
        >
          {menuItem.icon && (
            <Text font="icon" width={28} align={[0.5, 0.5]}>
              {menuItem.icon}
            </Text>
          )}{' '}
          {menuItem.text}
        </Button>
      ))}
    </Container>
  )
}

export default Menu
