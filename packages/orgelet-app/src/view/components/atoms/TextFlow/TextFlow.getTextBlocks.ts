import { TextStyle, TextMetrics } from 'pixi.js'

import { Vec } from 'orgelet-core/src/primitives'
import { TextStyleMap } from 'orgelet-core/src/fonts'
import { TextFlowBlock, TextFlowLine, TextFlowTextProps } from '.'

type TextFlowGetTextBlocksProps = {
  textItems: TextFlowTextProps[]
  textStyle: TextStyle
  textStyles: TextStyleMap
  maxWidth: number
  align: Vec
}

const shouldMergeTextItems = (
  textItem1: TextFlowTextProps,
  textItem2: TextFlowTextProps,
) => {
  if (textItem1.width !== undefined && textItem2.width !== undefined)
    return false
  if (textItem1.font !== textItem2.font) return false
  if (textItem2.align || textItem1.align) {
    if (textItem1.align && textItem2.align) {
      return (
        textItem1.align[0] === textItem2.align[0] &&
        textItem1.align[1] === textItem2.align[1]
      )
    }
    return false
  }
  return true
}

const getBlockOffset = (
  textItem: TextFlowTextProps,
  textWidth: number,
  lineOffset: Vec,
): Vec => {
  if (textItem.width !== undefined && textItem.align !== undefined) {
    return [
      lineOffset[0] + (textItem.width - textWidth) * textItem.align[0],
      lineOffset[1],
    ]
  }
  return [...lineOffset]
}

const getTextBlocks = ({
  textItems,
  textStyle,
  textStyles,
  maxWidth,
  align,
}: TextFlowGetTextBlocksProps): [TextFlowBlock[], Vec] => {
  let lines: TextFlowLine[] = []
  let line: TextFlowLine = {
    blocks: [],
    offset: [0, 0],
    maxHeight: 0,
    width: 0,
  }
  let maxLineWidth: number = 0

  const finishLine = () => {
    line.blocks = line.blocks.reduce<TextFlowBlock[]>((blocks, block) => {
      if (
        blocks.length > 0 &&
        shouldMergeTextItems(blocks[blocks.length - 1].textItem, block.textItem)
      ) {
        let b = blocks[blocks.length - 1]
        b = {
          ...blocks[blocks.length - 1],
          height: Math.max(b.height, block.height),
          textItem: {
            ...blocks[blocks.length - 1].textItem,
            children: b.textItem.children + block.textItem.children,
          },
        }
        return blocks.slice(0, blocks.length - 1).concat([b])
      }
      return blocks.concat([block])
    }, [])
    line.width = line.offset[0]
    lines.push(line)
    maxLineWidth = Math.max(maxLineWidth, line.width)
    line = {
      blocks: [],
      offset: [0, line.offset[1] + line.maxHeight],
      maxHeight: 0,
      width: 0,
    }
  }
  textItems.forEach((textItem) => {
    let ts = textStyle
    if (textItem.font) {
      if (!(textItem.font in textStyles))
        throw new Error(`Unknown font in TextFlow: ${textItem.font}`)
      ts = textStyles[textItem.font]
    }
    let { width, height } = TextMetrics.measureText(textItem.children, ts)
    let fullWidth = textItem.width ? textItem.width : width

    if (fullWidth > maxWidth - line.offset[0] && fullWidth < maxWidth) {
      finishLine()
      textItem = { ...textItem, children: textItem.children.trim() }
      ;({ width, height } = TextMetrics.measureText(textItem.children, ts))
      fullWidth = textItem.width ? textItem.width : width
    }

    line.blocks.push({
      offset: getBlockOffset(textItem, width, line.offset),
      textItem,
      height: height,
    })
    line.offset[0] += fullWidth
    line.maxHeight = Math.max(line.maxHeight, height)
  })
  finishLine()

  //process lines
  const blocks: TextFlowBlock[] = lines.reduce(
    (blocks: TextFlowBlock[], line: TextFlowLine) => {
      return blocks.concat(
        line.blocks.map((block) => ({
          ...block,
          offset: [
            block.offset[0] + (maxLineWidth - line.width) * align[0],
            block.offset[1] +
              (line.maxHeight - block.height) *
                (block.textItem.align ? block.textItem.align[1] : 0.5),
          ],
        })),
      )
    },
    [],
  )

  const textSize = [
    maxLineWidth,
    lines[lines.length - 1].offset[1] + lines[lines.length - 1].maxHeight,
  ]
  return [blocks, textSize]
}

export default getTextBlocks
