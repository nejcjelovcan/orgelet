import React, { useMemo, useContext, useEffect, ReactElement } from 'react'
import { Container, Text as PixiText } from '@inlet/react-pixi'

import { TextFlowProps, TextFlowTextProps } from '.'

import useTextStyles from '../../../../hooks/useTextStyles'
import useTextStyle from '../../../../hooks/useTextStyle'

import { BoxTextContext } from '../Box'
import getTextItems from './TextFlow.getTextItems'
import getTextBlocks from './TextFlow.getTextBlocks'

const getMax = (baseSize: number, maxSize?: number) => {
  if (baseSize && baseSize > 0) return baseSize
  if (maxSize && maxSize > 0) return maxSize
  return 500
}

export const Text = (props: TextFlowTextProps): ReactElement => ({
  type: 'font',
  props,
  key: null,
})

const TextFlow = ({
  offset,
  align = [0, 0],
  maxSize,
  fonts,
  children,
  ...rest
}: TextFlowProps) => {
  const textStyle = useTextStyle(rest)
  const textStyles = useTextStyles(fonts || {})
  const context = useContext(BoxTextContext)
  const baseSize = context.size || [0, 0]
  const maxWidth = getMax(baseSize[0], maxSize ? maxSize[0] : undefined)

  const textItems = useMemo(() => getTextItems(children), [children])
  const [textBlocks, textSize] = useMemo(
    () => getTextBlocks({ textItems, textStyle, textStyles, maxWidth, align }),
    [align, maxWidth, textItems, textStyle, textStyles],
  )

  useEffect(() => {
    if (context.setTextSize) {
      if (
        !context.textSize ||
        context.textSize[0] !== textSize[0] ||
        context.textSize[1] !== textSize[1]
      ) {
        context.setTextSize(textSize)
      }
    }
  }, [textSize, context])

  let offs = context.offset ? context.offset : offset || [0, 0]
  offs = [
    offs[0] + (baseSize[0] > 0 ? (baseSize[0] - textSize[0]) * align[0] : 0),
    offs[1] + (baseSize[1] > 0 ? (baseSize[1] - textSize[1]) * align[1] : 0),
  ]

  return (
    <Container x={offs[0]} y={offs[1]}>
      {textBlocks.map((textBlock, i) => (
        <PixiText
          key={i}
          x={textBlock.offset[0]}
          y={textBlock.offset[1]}
          text={textBlock.textItem.children}
          style={
            textBlock.textItem.font
              ? textStyles[textBlock.textItem.font]
              : textStyle
          }
        />
      ))}
    </Container>
  )
}
export default TextFlow
