import { ReactElement } from 'react'

import { Vec } from 'orgelet-core/src/primitives'
import { Font as FontDeclaration, FontMap } from 'orgelet-core/src/fonts'

import TextFlow from './TextFlow'
export { Text } from './TextFlow'
export default TextFlow

export type TextFlowTextProps = {
  font?: string
  width?: number
  align?: Vec
  children: string
}
export type TextFlowChild =
  | undefined
  | boolean
  | number
  | string
  | ReactElement<TextFlowTextProps>

export type TextFlowChildren = TextFlowChild | TextFlowChild[]

export type TextFlowProps = {
  offset?: Vec
  align?: Vec
  maxSize?: Vec
  fonts?: FontMap
  children: TextFlowChildren
} & Partial<FontDeclaration>

export type TextFlowBlock = {
  offset: Vec
  height: number
  textItem: TextFlowTextProps
}

export type TextFlowLine = {
  blocks: TextFlowBlock[]
  offset: Vec
  width: number
  maxHeight: number
}
