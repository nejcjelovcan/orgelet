import { Children, ReactElement } from 'react'
import { TextFlowChildren, TextFlowChild, TextFlowTextProps } from '.'

const getTextItems = (children?: TextFlowChildren) => {
  const items: TextFlowTextProps[] = []
  children !== undefined &&
    Children.forEach(children, (child: TextFlowChild) => {
      if (child) {
        if (typeof child === 'string' || typeof child === 'number') {
          ;(child === ' ' ? [' '] : child.toString().split(' ')).forEach(
            (part, i) =>
              items.push({
                children: i === 0 ? part : ` ${part}`,
              }),
          )
        } else {
          // TODO typing
          let c = child as ReactElement<TextFlowTextProps>
          c.props.children.split(' ').forEach((part, i) =>
            items.push({
              ...c.props,
              children: i === 0 ? part : ` ${part}`,
            }),
          )
          return c.props
        }
      }
    })
  return items
}

export default getTextItems
