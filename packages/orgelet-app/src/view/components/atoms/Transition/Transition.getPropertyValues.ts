import {
  TransitionablePropertiesRanges,
  TransitionablePropertiesValues,
  TransitionablePropertiesKey,
} from '.'

const lerp = (range: [number, number], percent: number): number =>
  range[0] + (range[1] - range[0]) * percent
const lerpPoint = (
  range: [[number, number], [number, number]],
  percent: number,
): [number, number] => [
  lerp([range[0][0], range[1][0]], percent),
  lerp([range[0][1], range[1][1]], percent),
]

// TODO typing situation is a bit awkward
// using the rest spread (even though exhaustive) breaks inferrence here
export default function getPropertyValues<T>(
  ranges: TransitionablePropertiesRanges<T>,
  percent: number,
): Partial<TransitionablePropertiesValues<T>> {
  const values: Partial<TransitionablePropertiesValues<T>> = {}
  Object.keys(ranges).forEach((propName) => {
    const range =
      ranges[(propName as unknown) as TransitionablePropertiesKey<T>]
    if (Array.isArray(range[0])) {
      ;(values as any)[
        (propName as unknown) as TransitionablePropertiesKey<T>
      ] = lerpPoint(range, percent)
    } else {
      ;(values as any)[
        (propName as unknown) as TransitionablePropertiesKey<T>
      ] = lerp(range, percent)
    }
  })
  return values
}
