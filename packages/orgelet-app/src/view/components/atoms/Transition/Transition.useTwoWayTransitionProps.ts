import {
  TransitionablePropertiesRanges,
  TransitionablePropertiesValues,
  UseTransitionProps,
  TransitionId,
  TransitionTwoWayState,
} from '.'
import { useState, useEffect } from 'react'
import { useTick } from '@inlet/react-pixi'
import getPropertyValues from './Transition.getPropertyValues'

export default function useTwoWayTransitionProps<T>({
  duration,
  id,
  ...properties
}: UseTransitionProps<T>): [
  Partial<TransitionablePropertiesValues<T>>,
  TransitionId,
  TransitionTwoWayState,
] {
  const [currentId, setCurrentId] = useState<TransitionId>(id)
  const [state, setState] = useState<TransitionTwoWayState>('start')
  const [time, setTime] = useState(0)
  const props = (properties as unknown) as TransitionablePropertiesRanges<T>

  const [values, setValues] = useState(() => getPropertyValues(props, 0))

  // on id change
  useEffect(() => {
    switch (state) {
      case 'start': // transition in if transitionId is not undefined
        if (id !== undefined) {
          setCurrentId(id)
          setState('in')
        }
        break
      case 'in':
      case 'end':
        // transition out if transitioning in or at end
        setState('out')
        break
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id])

  // on tick
  useTick((delta) => {
    if (delta) {
      switch (state) {
        case 'start':
          if (currentId) {
            setState('in')
          }
          break
        case 'in':
        case 'out':
          let t = time + (state === 'in' ? delta : -delta)
          if (t > duration) {
            t = duration
            setState('end')
          } else if (t < 0) {
            t = 0
            setState('start')
            if (id !== currentId) {
              setCurrentId(id)
            }
          }
          setTime(t)
          setValues(getPropertyValues(props, t / duration))
      }
    }
  })

  return [values, currentId, state]
}
