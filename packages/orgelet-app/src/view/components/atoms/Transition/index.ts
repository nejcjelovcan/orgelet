import { ReactElement } from 'react'

type PropertiesOfType<T, P> = Pick<
  T,
  {
    [K in keyof T]: T[K] extends P | undefined ? K : never
  }[keyof T]
>

export type TransitionablePropertiesValues<T> = PropertiesOfType<T, number> &
  PropertiesOfType<T, [number, number]>

export type TransitionablePropertiesKey<
  T
> = keyof TransitionablePropertiesValues<T>

export type TransitionablePropertiesRanges<T> = {
  [P in keyof PropertiesOfType<T, number>]: [number, number]
} &
  {
    [P in keyof PropertiesOfType<T, [number, number]>]: [
      [number, number],
      [number, number],
    ]
  }

export type TransitionId = string | number | boolean | undefined

export type TransitionChildCallback<T> = (
  props: Partial<TransitionablePropertiesValues<T>>,
) => ReactElement<T>

export type TransitionTwoWayProps<T> = UseTransitionProps<T> & {
  children: (
    props: Partial<TransitionablePropertiesValues<T>>,
  ) => ReactElement<T>
  debug?: T
} & Partial<TransitionablePropertiesRanges<T>>

export type TransitionState = 'start' | 'in' | 'end'

export type TransitionTwoWayState = 'start' | 'in' | 'end' | 'out'

export type UseTransitionProps<T> = {
  duration: number
  id?: TransitionId
} & Partial<TransitionablePropertiesRanges<T>>

export type WithTwoWayTransitionInnerProps<T> = {
  id?: TransitionId
  duration: number
  properties: Partial<TransitionablePropertiesRanges<T>>
}

export { default as useTransitionProps } from './Transition.useTransitionProps'
export { default as useTwoWayTransitionProps } from './Transition.useTwoWayTransitionProps'
export { default as withTwoWayTransition } from './withTwoWayTransition'
