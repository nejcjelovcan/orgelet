import {
  TransitionablePropertiesRanges,
  TransitionablePropertiesValues,
  UseTransitionProps,
  TransitionId,
  TransitionState,
} from '.'
import { useState, useEffect } from 'react'
import { useTick } from '@inlet/react-pixi'
import getPropertyValues from './Transition.getPropertyValues'

export default function useTransitionProps<T>({
  duration,
  id,
  ...properties
}: UseTransitionProps<T>): [
  Partial<TransitionablePropertiesValues<T>>,
  TransitionId,
  TransitionState,
] {
  const [currentId, setCurrentId] = useState<TransitionId>(id)
  const [state, setState] = useState<TransitionState>('start')
  const [time, setTime] = useState(0)
  const props = (properties as unknown) as TransitionablePropertiesRanges<T>

  const [values, setValues] = useState(() => getPropertyValues(props, 0))

  // on id change
  useEffect(() => {
    setCurrentId(currentId)
    setState('start')
    setTime(0)
    setValues(getPropertyValues(props, 0))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id])

  // on tick
  useTick((delta) => {
    if (delta) {
      switch (state) {
        case 'start':
          if (currentId) setState('in')
          break
        case 'in':
          let t = time + delta!
          if (t > duration) {
            t = duration
            setState('end')
          }
          setTime(t)
          setValues(getPropertyValues(props, t / duration))
      }
    }
  })

  return [values, currentId, state]
}

// export default function useTransitionValues<T>({
//   duration,
//   out,
//   id,
//   ...properties
// }: UseTransitionProps<T>): [
//   Partial<TransitionablePropertiesValues<T>>,
//   TransitionId,
// ] {
//   const [state, setState] = useState<TransitionState>('start')
//   const [currentId, setCurrentId] = useState<TransitionId>(id)
//   const [time, setTime] = useState(0)
//   // const [percent, setPercent] = useState(0)
//   const props = (properties as unknown) as TransitionablePropertiesRanges<T>

//   // TODO typing situation is a bit awkward
//   // using the rest spread (even though exhaustive) breaks inferrence here
//   const [values, setValues] = useState(() => {
//     const v: Partial<TransitionablePropertiesValues<T>> = {}
//     // TODO typing
//     Object.keys(props).forEach((propName) => {
//       ;(v as any)[(propName as unknown) as TransitionablePropertiesKey<T>] =
//         props[(propName as unknown) as TransitionablePropertiesKey<T>][0]
//     })
//     return v
//   })

//   // when transitionId changes
//   useEffect(() => {
//     switch (state) {
//       case 'start': // transition in if transitionId is not undefined
//         if (id !== undefined) {
//           console.log('setState(in)')
//           setCurrentId(id)
//           setState('in')
//         }
//         break
//       case 'in':
//       case 'end':
//         if (out) {
//           // transition out if transitioning in or at end
//           setState('out')
//           console.log('setState(out)')
//         } else {
//           // go to start and transition in
//           setValues(getNewValues(props, time / duration))
//           setCurrentId(id)
//           setState('in')
//         }
//         break
//     }
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, [id])

//   // on tick
//   useTick((delta) => {
//     if (delta) {
//       if (state === 'in' || state === 'out') {
//         let t = time + (state === 'in' ? delta! : -delta!)
//         let s: TransitionState = state
//         if (t > duration) {
//           t = duration
//           s = 'end'
//         }
//         if (t < 0) {
//           t = 0
//           s = 'start'
//         }
//         setTime(t)
//         setValues(getNewValues(props, t / duration))
//         if (s === 'start' && id !== undefined) {
//           setCurrentId(id)
//           setState('in')
//         } else if (s !== state) {
//           setState(s)
//         }
//       }
//     }
//   })

//   return [values, currentId]
// }
