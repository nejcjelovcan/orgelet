import React, { useState, useEffect } from 'react'
import { WithTwoWayTransitionInnerProps } from '.'
import useTwoWayTransitionProps from './Transition.useTwoWayTransitionProps'

export default function withTwoWayTransition<T>(
  Component: React.ComponentType<T>,
) {
  return (props: T & WithTwoWayTransitionInnerProps<T>) => {
    const { id, duration, properties, ...rest } = props
    const [currentPropsId, setCurrentPropsId] = useState(id)
    const [currentProps, setCurrentProps] = useState<T>((rest as unknown) as T)
    const [transitionProps, currentId, state] = useTwoWayTransitionProps<T>({
      id,
      duration,
      ...properties,
    })

    useEffect(() => {
      if (currentId !== currentPropsId) {
        setCurrentPropsId(currentId)
        setCurrentProps((rest as unknown) as T)
      }

      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentId])

    useEffect(() => {
      if (
        currentPropsId !== undefined &&
        (state === 'start' || state === 'end')
      ) {
        setCurrentProps((rest as unknown) as T)
      }

      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props])

    if (
      (currentId === undefined || currentPropsId === undefined) &&
      state !== 'end'
    ) {
      return null
    }
    return <Component {...currentProps} {...transitionProps} />
  }
}
