import React, { useState, useCallback } from 'react'

import { Vec } from 'orgelet-core/src/primitives'
import { Font } from 'orgelet-core/src/fonts'
import {
  GraphicsStyleStates,
  DisplayObjectOptions,
  GraphicsStyleOptions,
} from '../../core'

import Box from './Box'

import TextFlow, { TextFlowChildren } from './TextFlow'

export type ButtonProps = {
  align?: Vec
  callback?: () => void
  children: TextFlowChildren
} & Partial<Font> &
  DisplayObjectOptions &
  GraphicsStyleOptions &
  GraphicsStyleStates

const Button = ({
  align = [0, 0.5],
  callback,
  children,
  over,
  down,
  font,
  fontFamily,
  fontFill,
  fontLineHeight,
  fontOptions,
  fontSize,
  fontWeight,
  ...rest
}: ButtonProps) => {
  const [isOver, setIsOver] = useState(false)
  const [isDown, setIsDown] = useState(false)

  const onOver = useCallback(
    (e: PIXI.interaction.InteractionEvent) => setIsOver(true),
    [],
  )
  const onOut = useCallback(
    (e: PIXI.interaction.InteractionEvent) => setIsOver(false),
    [],
  )
  const onDown = useCallback(
    (e: PIXI.interaction.InteractionEvent) => setIsDown(true),

    [],
  )
  const onUp = useCallback(
    (e: PIXI.interaction.InteractionEvent) => {
      setIsDown(false)
      if (callback) callback()
    },
    [callback],
  )
  const defaultFont = {
    font,
    fontFamily,
    fontFill,
    fontLineHeight,
    fontOptions,
    fontSize,
    fontWeight,
  }

  const stateProps = {
    ...(down && isDown ? down : {}),
    ...(over && isOver ? over : {}),
  }

  return (
    <Box
      interactive
      pointerover={onOver}
      pointerout={onOut}
      pointerdown={onDown}
      pointerup={onUp}
      pointerupoutside={onUp}
      {...rest}
      {...stateProps}
    >
      <TextFlow
        align={align}
        fonts={{
          icon: { ...defaultFont, font: 'icon' },
          bold: { ...defaultFont, font: 'bold' },
        }}
        {...defaultFont}
      >
        {children}
      </TextFlow>
    </Box>
  )
}

export default Button
