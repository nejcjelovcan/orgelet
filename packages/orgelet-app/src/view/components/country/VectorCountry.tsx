import React from 'react'
import { Graphics } from '@inlet/react-pixi'

import { ProjectFunction } from 'orgelet-core/src/projection'
import { getGeoJsonPolygons } from 'orgelet-core/src/geojson'
import { getCountryGeometries } from 'orgelet-countries'
import { DisplayObjectOptions, GraphicsOptions } from '../../core'

type VectorCountryProps = {
  iso: string
  project: ProjectFunction
} & DisplayObjectOptions &
  GraphicsOptions

const VectorCountry = ({
  iso,
  project,

  fillColor = 0,
  fillAlpha = 1,

  lineColor = 0,
  lineAlpha = 1,
  linePosition = 0.5,
  lineType = 'normal',
  lineWidth = 1,

  ...rest
}: VectorCountryProps) => {
  if (!iso) return null

  return (
    <Graphics
      {...rest}
      draw={(g) => {
        g.clear()
        g.beginFill(fillColor, fillAlpha)
        g.lineStyle(
          lineWidth,
          lineColor,
          lineAlpha,
          linePosition,
          lineType === 'native',
        )
        // g.lineStyle(1.5, stroke, 1, 0.5, false)
        getGeoJsonPolygons(getCountryGeometries(iso), project).forEach(
          (polygon) => {
            g.drawPolygon(polygon)
          },
        )
        g.endFill()
      }}
    />
  )
}

export default VectorCountry
