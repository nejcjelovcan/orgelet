import React from 'react'

import { getCountryLabelPosition, getCountryData } from 'orgelet-countries'
import { ProjectFunction } from 'orgelet-core/src/projection'

import Box from '.././atoms/Box'
import TextFlow, { Text } from '.././atoms/TextFlow'

import { successLabelStyle, errorLabelStyle } from '../../styles'
import { DisplayObjectOptions, GraphicsStyleOptions } from '../../core'

export type CountryLabelProps = {
  iso: string
  project: ProjectFunction
  projectScale: number
  correct: boolean
} & DisplayObjectOptions &
  GraphicsStyleOptions

const CountryLabel = ({
  iso,
  project,
  projectScale,
  correct,
  ...rest
}: CountryLabelProps) => {
  // if (!iso) return null
  const [pos, left] = getCountryLabelPosition(iso)
  const countryPos = project(pos)
  const country = getCountryData(iso).properties

  const flagWidth = 30
  const emoji = country.emoji || ''
  const flag = emoji !== '' && (
    <Text font="flag" width={flagWidth} align={[left ? 1 : 0, 0.5]}>
      {country.emoji || ''}
    </Text>
  )
  const xMargin = 12

  const style = correct ? successLabelStyle : errorLabelStyle
  if (left && style.boxPadding && flag)
    style.boxPadding = [
      style.boxPadding[0],
      style.boxPadding[1],
      style.boxPadding[0] - 2,
      style.boxPadding[1],
    ]

  return (
    <Box
      position={[
        countryPos[0] * projectScale + (left ? -xMargin + 1 : xMargin),
        countryPos[1] * projectScale,
      ]}
      boxAnchor={[left ? 1 : 0, 0.5]}
      {...style}
      {...rest}
      draw={(g, { anchorOffset, actualSize, boxRadius }) => {
        if (emoji !== '') {
          g.lineStyle(0, 0)
          g.beginFill(0xffffff, 1)
          g.drawRoundedRect(
            left ? -flagWidth : anchorOffset[0],
            anchorOffset[1],
            flagWidth,
            actualSize[1],
            boxRadius,
          )
          g.endFill()
        }
      }}
    >
      <TextFlow
        font="label"
        fontFill={0xffffff}
        fonts={{
          flag: { font: 'labelBig', fontFill: 0xffffff },
        }}
      >
        {!left && flag}
        {country.NAME_LONG}
        {flag && ' '}
        {left && flag}
      </TextFlow>
    </Box>
  )
}

export default CountryLabel
