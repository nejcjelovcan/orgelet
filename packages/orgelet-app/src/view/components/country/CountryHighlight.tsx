import React from 'react'
import { Graphics, Container } from '@inlet/react-pixi'

import { ProjectFunction } from 'orgelet-core/src/projection'
import { getGeoJsonPolygons } from 'orgelet-core/src/geojson'
import { getCountryAltGeometries } from 'orgelet-countries'

import VectorCountry from './VectorCountry'
import { GraphicsOptions, DisplayObjectOptions } from '../../../view/core'

type CountryHighlightProps = {
  iso: string
  project: ProjectFunction
} & GraphicsOptions &
  DisplayObjectOptions

const CountryHighlight = ({
  iso,
  project,

  fillColor = 0,
  fillAlpha = 1,

  lineColor = 0xffffff,
  lineAlpha = 1,
  lineType = 'normal',
  lineWidth = 1.5,
  linePosition = 0.5,

  ...rest
}: CountryHighlightProps) => {
  const alt = getCountryAltGeometries(iso)

  return (
    <Container {...rest}>
      {alt.length > 0 && (
        <Graphics
          draw={(g) => {
            g.clear()
            g.beginFill(0, 0)
            g.lineStyle(
              lineWidth,
              lineColor,
              lineAlpha,
              0,
              lineType === 'native',
            )
            getGeoJsonPolygons(alt, project).forEach((polygon) =>
              g.drawPolygon(polygon),
            )
            g.endFill()
            g.lineStyle(
              lineWidth,
              fillColor,
              fillAlpha,
              1,
              lineType === 'native',
            )
            getGeoJsonPolygons(alt, project).forEach((polygon) =>
              g.drawPolygon(polygon),
            )
            g.endFill()
          }}
        />
      )}
      <VectorCountry
        iso={iso}
        project={project}
        {...{
          fillColor,
          fillAlpha,

          lineColor,
          lineAlpha,
          lineType,
          lineWidth,
          linePosition,
        }}
      />
    </Container>
  )
}
export default CountryHighlight
