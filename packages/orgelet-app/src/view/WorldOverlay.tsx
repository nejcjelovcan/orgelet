import React from 'react'
import { Container, withFilters } from '@inlet/react-pixi'
import { connect, ConnectedProps } from 'react-redux'
import { DropShadowFilter } from '@pixi/filter-drop-shadow'

import { Vec, BBox } from 'orgelet-core/src/primitives'
import { getCountryData } from 'orgelet-countries'

import { RootState } from '../redux/root.reducer'
import { gameNext } from '../redux/gameLoop'

import useWorldSpriteSubarea from '../hooks/useWorldSpriteSubarea'

import Button from './components/atoms/Button'
import Box from './components/atoms/Box'
import { buttonStyle, infoStyle } from './styles'
import TextFlow, { Text } from './components/atoms/TextFlow'
import CountryLabel from './components/country/CountryLabel'
import {
  withTwoWayTransition,
  WithTwoWayTransitionInnerProps,
} from './components/atoms/Transition'
import { DisplayObjectOptions } from './core'

const connector = connect((state: RootState) => ({ game: state.game }), {
  gameNext,
})
type WorldOverlayProps = ConnectedProps<typeof connector> & {
  size: Vec
  subarea?: BBox
}

const transitionDefaults: WithTwoWayTransitionInnerProps<DisplayObjectOptions> = {
  duration: 20,
  properties: { alpha: [0, 1] },
}
const TransitionBox = withTwoWayTransition(Box)
const TransitionButton = withTwoWayTransition(Button)
const TransitionCountryLabel = withTwoWayTransition(CountryLabel)

const Shadow = withFilters(Container, {
  shadow: DropShadowFilter,
})

const WorldOverlay = ({
  size,
  subarea,
  game: { answers, targets, length, state },
  gameNext,
}: WorldOverlayProps) => {
  const { offset, scale, fitSize, project } = useWorldSpriteSubarea({
    size,
    subarea,
  })
  const generalPadding = 10
  const iso = targets.length > 0 ? targets[targets.length - 1] : undefined
  const country = iso ? getCountryData(iso) : undefined

  const shadowProps = {
    quality: 5,
    resolution: 2,
    blur: 1,
    distance: 3,
    alpha: 0.6,
  }

  return (
    <Container x={offset[0]} y={offset[1]}>
      <Shadow shadow={shadowProps}>
        <TransitionBox
          {...transitionDefaults}
          id={state === 'question' || state === 'answer' ? iso : undefined}
          properties={{
            position: [
              [generalPadding, generalPadding - 70],
              [generalPadding, generalPadding],
            ],
            alpha: [0, 1],
          }}
          anchor={[0, 0]}
          {...infoStyle}
        >
          <TextFlow
            font="info"
            fontFill={0xffffff}
            fonts={{
              bold: { font: 'infoBold', fontFill: 0xffffff },
            }}
            maxSize={[800, 0]} // TODO
          >
            {targets.length}/{length} Find{' '}
            <Text font="bold">{`${country?.properties.NAME_LONG}`}</Text>
          </TextFlow>
        </TransitionBox>
      </Shadow>

      <Shadow shadow={shadowProps}>
        <TransitionButton
          id={state === 'answer' ? iso : undefined}
          {...transitionDefaults}
          properties={{
            position: [
              [fitSize[0] - generalPadding, generalPadding - 70],
              [fitSize[0] - generalPadding, generalPadding],
            ],
            alpha: [0, 1],
          }}
          boxAnchor={[1, 0]}
          align={[0, 0.5]}
          callback={() => gameNext()}
          {...buttonStyle}
        >
          NEXT <Text font="icon">{'\uf105'}</Text>
        </TransitionButton>
      </Shadow>

      <Shadow shadow={shadowProps}>
        <TransitionCountryLabel
          id={state === 'answer' ? iso : undefined}
          {...transitionDefaults}
          iso={iso!}
          project={project}
          projectScale={scale}
          correct={answers[answers.length - 1] === targets[targets.length - 1]}
        />
      </Shadow>
    </Container>
  )
}

export default connector(WorldOverlay)
