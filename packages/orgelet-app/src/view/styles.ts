import { earthPalette } from 'orgelet-core/src/colors'

import { GraphicsStyleOptions, GraphicsStyleStates } from './core'

export const buttonNormalStyle: GraphicsStyleOptions = {
  boxRadius: 5,
  boxPadding: [6, 1, 6, 3],
  fillColor: earthPalette.blue,
  lineColor: 0xffffff,
  font: 'menu',
  fontFill: 0xffffff,
}

export const buttonOverStyle: GraphicsStyleOptions = {
  ...buttonNormalStyle,
  fillColor: earthPalette.green,
}

export const buttonDownStyle: GraphicsStyleOptions = {
  ...buttonNormalStyle,
  fillColor: earthPalette.yellow,
}

export const buttonStyle: GraphicsStyleStates = {
  ...buttonNormalStyle,
  over: buttonOverStyle,
  down: buttonDownStyle,
}

export const infoStyle: GraphicsStyleOptions = {
  boxRadius: 5,
  boxPadding: [10, 4, 10, 6],
  fillColor: earthPalette.pink,
  fillAlpha: 0.9,
  lineColor: 0xffffff,
  font: 'info',
  fontFill: 0xffffff,
}

export const infoIconStyle: GraphicsStyleOptions = {
  ...infoStyle,
  font: 'icon',
}

export const successLabelStyle: GraphicsStyleOptions = {
  boxRadius: 5,
  boxPadding: [5, 0],
  fillColor: earthPalette.green,
  lineColor: 0xffffff,
  font: 'label',
  fontFill: 0xffffff,
}

export const errorLabelStyle: GraphicsStyleOptions = {
  ...successLabelStyle,
  fillColor: earthPalette.red,
}
