// const path = require('path')

const getYarnWorkspaces = require('get-yarn-workspaces')
const {
  override,
  babelInclude,
  // addDecoratorsLegacy,
  // disableEsLint,
  // addBabelPlugin,
  // addWebpackAlias,
} = require('customize-cra')

module.exports = override(
  // addDecoratorsLegacy(),
  // disableEsLint(),
  babelInclude(getYarnWorkspaces()),
  // addBabelPlugin('babel-plugin-parameter-decorator'),
  // addWebpackAlias({
  //   'type-graphql': path.resolve(
  //     __dirname,
  //     '../../node_modules/type-graphql/dist/browser-shim.js',
  //   ),
  // }),
)
