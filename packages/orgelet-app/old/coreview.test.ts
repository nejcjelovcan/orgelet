import { fitViewport } from './view'

describe('fitViewport', () => {
  it('returns scale and offset for the content fit', () => {
    expect(
      fitViewport({ size: [600, 400], contentSize: [1200, 800] }),
    ).toMatchObject({ scale: 0.5, offset: [0, 0] })
  })
  it('returns scale and offset for the content fit (portrait)', () => {
    expect(
      fitViewport({ size: [400, 800], contentSize: [1000, 800] }),
    ).toMatchObject({ scale: 0.4, offset: [0, 240] })
  })
  it('returns scale and offset for the content fit (landscape)', () => {
    expect(
      fitViewport({ size: [800, 400], contentSize: [1000, 800] }),
    ).toMatchObject({ scale: 0.5, offset: [150, 0] })
  })
})
