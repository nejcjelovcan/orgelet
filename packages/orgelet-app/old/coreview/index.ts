import { Font } from '../fonts'
import { Vec } from '../primitives'
import { DisplayObject } from 'pixi.js'
import { _ReactPixi } from '@inlet/react-pixi'

export type LineType = 'normal' | 'native'

export type LineOptions = {
  lineWidth: number
  lineType: LineType
  lineColor: number
  lineAlpha: number
  linePosition: number
}

export type FillOptions = {
  fillColor: number
  fillAlpha: number
}

export type BoxOptions = {
  boxAnchor: Vec
  boxPadding: Vec
  boxSize: Vec
  boxRadius: number
}

// (select ones that are intended to be propagated)
type WithPoint<T extends keyof any> = { [P in T]: [number, number] }
type P = 'anchor' | 'position' | 'scale' | 'pivot' | 'skew'
export type IDisplayObject = Pick<
  DisplayObject,
  'alpha' | 'interactive' | 'scale' | 'rotation' | 'visible' | 'zIndex'
> &
  WithPoint<P> &
  _ReactPixi.InteractionEvents

export type DisplayObjectOptions = Partial<IDisplayObject>

export type GraphicsOptions = Partial<LineOptions> & Partial<FillOptions>

export type GraphicsStyleOptions = GraphicsOptions &
  Partial<BoxOptions> &
  Partial<Font>

export type GraphicsStyleStates = {
  over?: GraphicsStyleOptions
  down?: GraphicsStyleOptions
}

export { makeDrawBackground, fitViewport } from './view.helpers'
