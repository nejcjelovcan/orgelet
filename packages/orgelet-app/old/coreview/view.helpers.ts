import { Graphics } from 'pixi.js'

import { RenderProjection, getViewSize } from '../projection'
import { palette } from '../colors'
import { Vec } from '../primitives'

export const fitViewport = ({
  size,
  contentSize,
}: {
  size: Vec
  contentSize: Vec
}) => {
  const sizeRatio = size[1] / size[0]
  const contentRatio = contentSize[1] / contentSize[0]
  if (contentRatio > sizeRatio) {
    return {
      offset: [(size[1] / sizeRatio - size[1] / contentRatio) / 2, 0],
      scale: size[1] / contentSize[1],
    }
  } else {
  }
  return {
    offset: [0, (size[0] * sizeRatio - size[0] * contentRatio) / 2],
    scale: size[0] / contentSize[0],
  }
}
