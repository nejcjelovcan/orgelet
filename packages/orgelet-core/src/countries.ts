import { Geometry, Feature, FeatureCollection } from 'geojson'

export type CountriesLists = {
  sovereign: string[]
  small: string[]
  dataMap: { [name: string]: number }
  geometryMap: { [name: string]: number[] }
  altGeometryMap: { [name: string]: number[] }
}

export type CountryMinimalGeojsonProperties = {
  ISO_A2: string
}

export type CountryGeojsonProperties = CountryMinimalGeojsonProperties & {
  ADMIN: string
  NAME: string
  NAME_LONG: string
  POP_EST: number
  POP_YEAR: number
  GDP_MD_EST: number
  GDP_YEAR: number
  CONTINENT: string
  WOE_ID: number
  TYPE: string
  SOVEREIGNT: string
  emoji?: string

  // TODO stuff below is for orgelet-countries, need to move it
  min_zoom: number
  direction: string
  degrees: number
}

export type CountryDataFeature = Feature<null, CountryGeojsonProperties>
export type CountryDataFeatureCollection = FeatureCollection<
  null,
  CountryGeojsonProperties
>

export type CountryGeometryFeature = Feature<
  Geometry,
  CountryMinimalGeojsonProperties
>
export type CountryGeometryFeatureCollection = FeatureCollection<
  Geometry,
  CountryMinimalGeojsonProperties
>

export type CountryFullFeature = Feature<Geometry, CountryGeojsonProperties>
export type CountryFullFeatureCollection = FeatureCollection<
  Geometry,
  CountryGeojsonProperties
>
