import IGameParticipation from './IGameParticipation'

export default interface IGame {
  uuid: string
  type: GameType
  area: GameArea
  state: GameState
  order: GameOrder
  length: number
  participations: IGameParticipation[]
  targets: string[]
  createdAt: Date
  updatedAt: Date
}

export enum GameType {
  Countries = 'countries',
  Small = 'small',
}

export enum GameArea {
  World = 'world',
}

export enum GameState {
  Idle = 'idle',
  Question = 'question',
  Answer = 'answer',
  Next = 'next',
  Over = 'over',
}

export enum GameOrder {
  Random = 'random',
  Asc = 'asc',
  Desc = 'desc',
}
