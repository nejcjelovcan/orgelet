import IUser from './IUser'
import IGame from './IGame'

export default interface IGameParticipation {
  uuid: string
  isHost: boolean
  participant: IUser
  participantUuid: string
  game: IGame
  gameUuid: string
  answers: string[]
  points: number[]
  createdAt: Date
  updatedAt: Date
}
