import IGameParticipation from './IGameParticipation'

export default interface IUser {
  uuid: string
  alias: string
  participations: IGameParticipation[]
  token?: String
  createdAt: Date
  updatedAt: Date
}
