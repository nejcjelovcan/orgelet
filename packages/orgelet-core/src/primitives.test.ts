import { getBBoxSize } from './primitives'

describe('getBBoxSize', () => {
  it('Returns size of given rect', () => {
    expect(getBBoxSize([0, 0, 10, 10])).toMatchObject([10, 10])
    expect(getBBoxSize([10.99, 10.99, 100.01, 100.01])).toMatchObject([
      100.01 - 10.99,
      100.01 - 10.99,
    ])
  })
  it('Returns size for given rect with topLeft > bottomRight', () => {
    expect(getBBoxSize([10, 10, 0, 0])).toMatchObject([-10, -10])
  })
})
