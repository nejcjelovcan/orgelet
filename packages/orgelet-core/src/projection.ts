import { toMercator, toWgs84 } from '@turf/projection'
import { Vec, BBox, getBBoxSize } from './primitives'

export type RenderProjection = {
  coordinateRect: BBox
  viewRect: BBox
}

export type ProjectFunction = (lonlat: Vec) => Vec

export const projectRect = (project: ProjectFunction, rect: BBox): BBox => {
  const p1 = project([rect[0], rect[1]])
  const p2 = project([rect[2], rect[3]])
  return [p1[0], p1[1], p2[0], p2[1]]
}

export const calculateNaturalRatio = ({
  coordinateRect,
}: {
  coordinateRect: BBox
}) => {
  const mercatorRect = projectRect(toMercator, coordinateRect)
  const mercatorSize = getBBoxSize(mercatorRect)
  return Math.abs(mercatorSize[1]) / Math.abs(mercatorSize[0])
}

export const getViewSize = ({ viewRect }: RenderProjection): number[] =>
  getBBoxSize(viewRect)

export const getMercatorSize = ({
  coordinateRect,
}: RenderProjection): number[] =>
  getBBoxSize(projectRect(toMercator, coordinateRect))

export const getRenderProjectionScale = (
  renderProjection: RenderProjection,
): number[] => {
  const viewSize = getViewSize(renderProjection)
  const mercatorSize = getMercatorSize(renderProjection)
  return [viewSize[0] / mercatorSize[0], viewSize[1] / mercatorSize[1]]
}

export const getRenderProjectionFunction = (
  renderProjection: RenderProjection,
): ProjectFunction => {
  const mercatorRect = projectRect(toMercator, renderProjection.coordinateRect)
  const scale = getRenderProjectionScale(renderProjection)
  return (lonlat: Vec): Vec => {
    const merc = toMercator(
      [Math.min(180, Math.max(-180, lonlat[0])), -lonlat[1]],
      {
        mutate: true,
      },
    )
    return [
      (merc[0] - mercatorRect[0]) * scale[0],
      (merc[1] + mercatorRect[3]) * scale[1],
    ]
  }
}

export const getRenderProjectionReverseFunction = (
  renderProjection: RenderProjection,
): ProjectFunction => {
  const mercatorRect = projectRect(toMercator, renderProjection.coordinateRect)
  const scale = getRenderProjectionScale(renderProjection)
  return (lonlat: Vec): Vec => {
    const merc = toWgs84([
      lonlat[0] / scale[0] + mercatorRect[0],
      -(lonlat[1] / scale[1] - mercatorRect[3]),
    ])
    return [merc[0], merc[1]]
  }
}

export const getRenderProjectionSubarea = (
  renderProjection: RenderProjection,
  subareaCoordinateRect: BBox,
): BBox => {
  const project = getRenderProjectionFunction(renderProjection)
  return projectRect(project, subareaCoordinateRect)
}
