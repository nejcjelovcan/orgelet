import { BBox as OBBox } from '@turf/helpers'

export type Vec = number[]
export type Shape = Vec[]
export type Polygon = number[]
export type BBox = number[]

export const getBBoxSize = (bbox: BBox): Vec => [
  bbox[2] - bbox[0],
  bbox[3] - bbox[1],
]

export const getPointBox = (point: Vec, bbox: BBox): BBox => [
  point[0] + bbox[0],
  point[1] + bbox[1],
  point[0] + bbox[2],
  point[1] + bbox[3],
]
