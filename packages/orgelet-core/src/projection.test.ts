import {
  calculateNaturalRatio,
  getViewSize,
  getMercatorSize,
  getRenderProjectionScale,
  getRenderProjectionFunction,
  RenderProjection,
} from './projection'
import { BBox } from './primitives'

const VIEW_RECT: BBox = [-10, -10, 90, 90]
const COORDINATE_RECT: BBox = [-180, -60, 180, 60]
const RENDER_PROJECTION: RenderProjection = {
  viewRect: VIEW_RECT,
  coordinateRect: COORDINATE_RECT,
}

describe('calculateNaturalRatio', () => {
  it('calculates natural ratio for given coordinate rect', () => {
    expect(calculateNaturalRatio({ coordinateRect: COORDINATE_RECT })).toBe(
      0.4192007182789826,
    )
  })
})

describe('getViewSize', () => {
  it("calculates size of renderProjection's viewRect", () => {
    expect(getViewSize(RENDER_PROJECTION)).toMatchObject([100, 100])
  })
})

describe('getMercatorSize', () => {
  it("calculates mercator size of renderProjection's coordinateRect", () => {
    expect(getMercatorSize(RENDER_PROJECTION)).toMatchObject([
      40075016.68557849,
      16799475.779636715,
    ])
  })
})

describe('getRenderProjectionScale', () => {
  it('calculates scale for coordinateRect to fit viewRect', () => {
    expect(getRenderProjectionScale(RENDER_PROJECTION)).toMatchObject([
      0.0000024953202336653373,
      0.0000059525666938496865,
    ])
  })
})

describe('getRenderProjectionFunction', () => {
  it('returns function for a render projection', () => {
    const project = getRenderProjectionFunction(RENDER_PROJECTION)
    expect(project([-180, -60])).toMatchObject([0, 99.99999999999999])
    expect(project([180, 60])).toMatchObject([100, -2.2175038955545747e-14])
  })
})
