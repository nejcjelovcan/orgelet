import { CountriesLists } from './countries'

// TODO this is mostly getting reimplemented in orgelet-model

export type GameType = 'countries' | 'small'
export type GameArea = 'world'
export type GameState = 'idle' | 'question' | 'answer' | 'next' | 'over'
export type GameOrder = 'random' | 'asc' | 'desc'

export type Game = {
  type: GameType
  area: GameArea
  state: GameState
  order: GameOrder
  length: number
  targets: string[]
  answers: string[]
  points: number[]
}

export const GameDefaults: Game = {
  type: 'countries',
  area: 'world',
  state: 'idle',
  order: 'random',
  length: 20,
  targets: [],
  answers: [],
  points: [],
}

export const getGameCountrySet = (
  countriesLists: CountriesLists,
  {
    type,
    area,
  }: {
    type: GameType
    area: GameArea
  },
): string[] => {
  switch (type) {
    case 'countries':
      return countriesLists.sovereign
    case 'small':
      return countriesLists.small
    default:
      throw new Error('Not implemented!')
  }
}

export const getNextGameQuestion = (
  countriesLists: CountriesLists,
  { type, order, area, targets }: Game,
) => {
  const isos = getGameCountrySet(countriesLists, { type, area })
  const nonVisited = isos.filter((iso) => !targets.includes(iso))
  switch (order) {
    case 'asc':
      return nonVisited[0]
    case 'desc':
      return nonVisited[nonVisited.length - 1]
    default:
      return nonVisited[Math.floor(Math.random() * nonVisited.length)]
  }
}
