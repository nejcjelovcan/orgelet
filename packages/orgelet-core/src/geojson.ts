import { Feature } from 'geojson'
//
import { Vec, Shape } from './primitives'
import { ProjectFunction } from './projection'

// get shapes of a feature
// (handles different geojson geometry types)
const getGeoJsonShapes = (
  feature: Feature,
  project?: ProjectFunction,
): Shape[] => {
  const geometry = feature.geometry
  if (!geometry) return []
  if (!project) project = (coords) => coords
  switch (geometry.type) {
    case 'Polygon':
      return [geometry.coordinates[0].map(project!)]
    case 'LineString':
      return [geometry.coordinates.map(project!)]
    case 'MultiLineString':
      return [geometry.coordinates[0].map(project!)]
    case 'MultiPolygon':
      return geometry.coordinates.map((polygon) => polygon[0].map(project!))
    default:
      throw new Error('Geometry type not implemented: ' + geometry.type)
  }
}

export const getGeoJsonPolygons = (
  feature: Feature | Feature[],
  project?: ProjectFunction,
  origin?: Vec,
): number[][] =>
  (Array.isArray(feature) ? feature : [feature])
    .map((f) => getGeoJsonShapes(f, project).map((shape) => shape.flat()))
    .reduce((list, items) => list.concat(items), [])
