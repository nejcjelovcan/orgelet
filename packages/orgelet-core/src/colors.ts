import chroma from 'chroma-js'

export const COLOR_WATER = chroma('#1D6996').num()
export const COLOR_COUNTRY = chroma('#0F8554').num()
export const COLOR_COUNTRY_OVER = chroma('#73AF48').num()
export const COLOR_LINE = chroma('#FFFFFF').num()

const toNum = (color: string) => chroma(color).num()

export const COLORS = [
  '#5F4690',
  '#1D6996',
  '#38A6A5',
  '#0F8554',
  '#73AF48',
  '#EDAD08',
  '#E17C05',
  '#CC503E',
  '#94346E',
  '#6F4070',
  '#994E95',
  '#666666',
  '#999999',
  '#DDDDDD',
].map(toNum)

export const earthPalette = {
  green: toNum('#31a354'),
  greens: ['#99d8c9', '#31a354', '#0F8554', '#73AF48'].map(toNum),
  red: toNum('#CC503E'),
  pink: toNum('#94346E'),
  blue: toNum('#38A6A5'),
  yellow: toNum('#EDAD08'),
  orange: toNum('#E17C05'),
  gray: toNum('#7a7267'),
  // gray: toNum('#4f4d42'),
  waterColors: [
    '#9ecae1',
    '#8cbad5',
    '#7aaaca',
    '#699abf',
    '#598ab3',
    '#4a7ba8',
    '#3b6b9c',
    '#2e5c90',
    '#214d84',
    '#153e78',
    '#08306b',
  ].map(toNum),
}

export const palette = {
  blue: {
    100: chroma('#6D799A').num(),
    300: chroma('#435481').num(),
    500: chroma('#243668').num(),
    700: chroma('#0E1F4F').num(),
    900: chroma('#041237').num(),
  },
  red: {
    100: chroma('#D098AF').num(),
    300: chroma('#B05E80').num(),
    500: chroma('#913059').num(),
    700: chroma('#720F39').num(),
    900: chroma('#520023').num(),
  },
  green: {
    100: chroma('#C4DEA2').num(),
    300: chroma('#96BD64').num(),
    500: chroma('#6E9B34').num(),
    700: chroma('#4C7A10').num(),
    900: chroma('#325800').num(),
  },
  yellow: {
    100: chroma('#FFF2D6').num(),
    300: chroma('#FFDE97').num(),
    500: chroma('#DFB357').num(),
    700: chroma('#C59227').num(),
    900: chroma('#9B6C09').num(),
  },
}
