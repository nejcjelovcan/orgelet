import { TextStyle } from 'pixi.js'
import chroma from 'chroma-js'

// infer TextStyle options (first argument of TextStyle constructor)
type FirstArgument<T> = T extends (arg1: infer U, ...args: any[]) => any
  ? U
  : any

export type TextStyleOptions = FirstArgument<typeof TextStyle>

export const textStyleDefaults: TextStyleOptions = {
  fontFamily: 'Cabin, Helvetica, Arial, sans-serif',
  fontSize: 16,
  fontWeight: '400',
}

export const styles = {
  normal: {
    fontSize: 12,
    fontWeight: '400',
  },
  bold: {
    fontWeight: '400',
  },
  menu: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: '400',
  },
  info: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: '400',
  },
  infoBold: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: '700',
  },
  icon: {
    fontFamily: 'FontAwesome',
    fontSize: 24,
    lineHeight: 32,
    fontWeight: '400',
  },
  label: {
    fontSize: 16,
    lineHeight: 16,
    fontWeight: '400',
  },
  labelBig: {
    fontSize: 20,
    lineHeight: 20,
    fontWeight: '400',
  },
}
export type FontName = keyof typeof styles

export type Font = {
  font: FontName
  fontFamily: string
  fontSize: number
  fontLineHeight: number
  fontWeight: string
  fontFill: number
  fontOptions: TextStyleOptions
}

export type FontMap = { [name: string]: Partial<Font> }

export type TextStyleMap = { [name: string]: TextStyle }

export const makeTextStyle = ({
  font = 'normal',
  fontFamily,
  fontSize,
  fontLineHeight,
  fontWeight,
  fontFill,
  fontOptions,
}: Partial<Font>): TextStyle => {
  const props: Partial<TextStyle> = {}
  // TODO
  if (fontFamily) props.fontFamily = fontFamily
  if (fontSize) props.fontSize = fontSize
  if (fontLineHeight) props.lineHeight = fontLineHeight
  if (fontWeight) props.fontWeight = fontWeight
  if (fontFill) props.fill = chroma(fontFill).hex()

  return new TextStyle({
    ...textStyleDefaults,
    ...styles[font],
    ...{
      lineHeight: fontLineHeight,
      fill: fontFill !== undefined ? chroma(fontFill).hex() : undefined,
    },
    ...props,
    ...fontOptions,
  })
}
