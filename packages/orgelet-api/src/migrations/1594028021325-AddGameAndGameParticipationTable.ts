import { MigrationInterface, QueryRunner } from 'typeorm'

export class AddGameAndGameParticipationTable1594028021325
  implements MigrationInterface {
  name = 'AddGameAndGameParticipationTable1594028021325'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "game_participation" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "isHost" boolean NOT NULL DEFAULT false, "participantUuid" uuid NOT NULL, "gameUuid" uuid NOT NULL, "answers" jsonb NOT NULL DEFAULT '[]', "points" jsonb NOT NULL DEFAULT '[]', "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_0d4031895c65da14b19ab024581" PRIMARY KEY ("uuid"))`,
    )
    await queryRunner.query(
      `CREATE TABLE "game" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "type" character varying NOT NULL DEFAULT 'countries', "area" character varying NOT NULL DEFAULT 'world', "state" character varying NOT NULL DEFAULT 'idle', "order" character varying NOT NULL DEFAULT 'random', "length" integer NOT NULL DEFAULT 10, "targets" jsonb NOT NULL DEFAULT '[]', "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_6ee95d182173a211ff700a021ba" PRIMARY KEY ("uuid"))`,
    )
    await queryRunner.query(
      `ALTER TABLE "game_participation" ADD CONSTRAINT "FK_d587848649ce4c46582c5c79126" FOREIGN KEY ("participantUuid") REFERENCES "user"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    )
    await queryRunner.query(
      `ALTER TABLE "game_participation" ADD CONSTRAINT "FK_a7a3bec787a8d6496ca8956f433" FOREIGN KEY ("gameUuid") REFERENCES "game"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "game_participation" DROP CONSTRAINT "FK_a7a3bec787a8d6496ca8956f433"`,
    )
    await queryRunner.query(
      `ALTER TABLE "game_participation" DROP CONSTRAINT "FK_d587848649ce4c46582c5c79126"`,
    )
    await queryRunner.query(`DROP TABLE "game"`)
    await queryRunner.query(`DROP TABLE "game_participation"`)
  }
}
