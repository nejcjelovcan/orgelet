import 'reflect-metadata'

import { config } from 'dotenv'
import { createServer } from 'http'
import express from 'express'
import passport from 'passport'
import cors from 'cors'
import compression from 'compression'
import bodyParser from 'body-parser'

import newApolloServer from './apollo/newApolloServer'

import strategy from './authentication/strategy'

config()
const { PORT } = process.env
passport.use(strategy)

async function main() {
  const server = await newApolloServer()
  const app = express()
  app.use(passport.initialize())
  app.use('*', cors())
  app.use(compression())
  app.use(bodyParser.json())

  // app.use('/graphql', passport.authenticate('jwt', { session: false }))
  app.use('/graphql', (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (!err && user) {
        req.user = user
      } // TODO else

      next()
    })(req, res, next)
  })
  server.applyMiddleware({ app, path: '/graphql' })

  const httpServer = createServer(app)
  httpServer.listen({ port: PORT }, (): void =>
    console.log(
      `\n🚀      GraphQL is now running on http://localhost:${PORT}/graphql`,
    ),
  )
}

main()
