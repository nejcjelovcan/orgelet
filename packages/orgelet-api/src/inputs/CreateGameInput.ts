import { InputType, Field } from 'type-graphql'

import { GameType, GameArea, GameOrder } from 'orgelet-core/src/model/IGame'

@InputType()
export default class CreateGameInput {
  @Field((type) => GameType, { defaultValue: GameType.Countries })
  type?: GameType

  @Field((type) => GameArea, { defaultValue: GameArea.World })
  area?: GameArea

  @Field((type) => GameOrder, { defaultValue: GameOrder.Random })
  order?: GameOrder

  @Field({ defaultValue: 10 })
  length?: number
}
