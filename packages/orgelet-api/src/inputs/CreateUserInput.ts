import { InputType, Field } from 'type-graphql'

@InputType()
export default class CreateUserInput {
  @Field()
  alias: string
}
