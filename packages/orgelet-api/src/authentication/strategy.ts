import { readFileSync } from 'fs'
import { join } from 'path'
import { Strategy, ExtractJwt } from 'passport-jwt'

const SECRET_KEY = readFileSync(
  join(__dirname, '../../../../secret/orgelet-jwt-key'),
  'utf8',
)

const PUB_KEY = readFileSync(
  join(__dirname, '../../../../secret/orgelet-jwt-key.pub'),
  'utf8',
)

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET_KEY,
}

const strategy = new Strategy(options, function (jwt_payload, done) {
  if (jwt_payload && jwt_payload.uuid) {
    done(null, { uuid: jwt_payload.uuid })
  } else {
    done('No User uuid found in JWT!', false)
  }
})
export default strategy
