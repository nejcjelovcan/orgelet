import { readFileSync } from 'fs'
import { join } from 'path'
import jwt from 'jsonwebtoken'
import { Resolver, Query, Mutation, Arg, Authorized, Ctx } from 'type-graphql'

import IContext from 'orgelet-core/src/model/IContext'

import User from '../entity/User'

const SECRET_KEY = readFileSync(
  join(__dirname, '../../../../secret/orgelet-jwt-key'),
  'utf8',
)

@Resolver()
export default class UserResolver {
  @Authorized()
  @Query(() => [User])
  users() {
    return User.find()
  }

  @Authorized()
  @Query(() => User, { nullable: true })
  currentUser(@Ctx() ctx: IContext) {
    return User.findOne(ctx.user!.uuid)
  }

  @Mutation(() => User)
  async login(@Arg('alias') alias: string) {
    const user = User.create({ alias })
    await user.save()
    const token = jwt.sign({ uuid: user.uuid }, SECRET_KEY)
    return { ...user, token }
  }

  // @Mutation(() => User)
  // async createUser(@Arg('data') data: CreateUserInput) {
  //   const user = User.create(data)
  //   await user.save()
  //   return user
  // }
}

// app.post('/get-token', async (req, res) => {
//   //   // User.create()
//   //   const { alias } = req.body
//   //   // const user = users.find(user => user.email === email)
//   //   if (alias) {
//   //     const user = User.create({ alias })
//   //     await user.save()
//   //     const token = jwt.sign({ uuid: user.uuid }, SECRET_KEY)
//   //     res.send({
//   //       success: true,
//   //       token: token,
//   //     })
//   //   } else {
//   //     //return error to user to let them know the account there are using does not exists
//   //     res.status(400).send({
//   //       success: false,
//   //       message: `Alias must be provided`,
//   //     })
//   //   }
//   // })
