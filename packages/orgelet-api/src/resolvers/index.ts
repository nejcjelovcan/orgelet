import UserResolver from './UserResolver'
import GameResolver from './GameResolver'

const resolvers = [UserResolver, GameResolver]

export default resolvers
