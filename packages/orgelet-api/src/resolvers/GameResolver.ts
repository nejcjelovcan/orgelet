import { Resolver, Query, Mutation, Arg, Ctx, Authorized } from 'type-graphql'

import IContext from 'orgelet-core/src/model/IContext'

import Game from '../entity/Game'
import CreateGameInput from '../inputs/CreateGameInput'
import GameParticipation from '../entity/GameParticipation'
import { GameState } from 'orgelet-core/src/model/IGame'

@Resolver()
export default class GameResolver {
  @Query(() => [Game])
  async games() {
    return Game.find({
      relations: ['participations', 'participations.participant'],
    })
  }

  @Authorized()
  @Query(() => Game, { nullable: true })
  async currentGame(@Ctx() ctx: IContext) {
    const part = await GameParticipation.createQueryBuilder(
      'game_participation',
    )
      .innerJoin('game_participation.game', 'game')
      .where(
        'game.state != :state AND game_participation.participantUuid = :uuid',
        {
          state: GameState.Over,
          uuid: ctx!.user?.uuid,
        },
      )
      .orderBy('game_participation.createdAt', 'DESC')
      .getOne()

    if (part) {
      return Game.findOne({
        where: { uuid: part.gameUuid },
        relations: ['participations', 'participations.participant'],
      })
    }
    return null
  }

  @Authorized()
  @Mutation(() => Game)
  async createGame(@Arg('data') data: CreateGameInput, @Ctx() ctx: IContext) {
    const game = Game.create(data)
    await game.save()
    const hostParticipation = GameParticipation.create({
      isHost: true,
      participant: ctx.user!,
      // participantUuid: ctx.user!.uuid,
      gameUuid: game.uuid,
    })
    await hostParticipation.save()
    return game
  }
}
