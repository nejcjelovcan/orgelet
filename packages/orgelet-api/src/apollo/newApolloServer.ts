import { buildSchema } from 'type-graphql'
import { ApolloServer } from 'apollo-server-express'
import { createConnection } from 'typeorm'

import resolvers from '../resolvers/'
import authChecker from '../authentication/authChecker'

export default async function newApolloServer() {
  const connection = await createConnection()
  const schema = await buildSchema({
    resolvers,
    authChecker,
  })
  const server = new ApolloServer({
    schema,
    context: ({ req, res }) => ({ user: req.user }),
    playground: { settings: { 'request.credentials': 'include' } },
  })

  return server
}
