import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm'
import { ObjectType, Field, ID } from 'type-graphql'

import IGameParticipation from 'orgelet-core/src/model/IGameParticipation'

import User from './User'
import Game from './Game'

@Entity()
@ObjectType()
export default class GameParticipation extends BaseEntity
  implements IGameParticipation {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  uuid: string

  @Field(() => Boolean)
  @Column({ nullable: false, default: false })
  isHost: boolean

  @Field((type) => User)
  @ManyToOne((type) => User, (user) => user.participations)
  participant: User
  @Column({ nullable: false })
  participantUuid: string

  @Field((type) => Game)
  @ManyToOne((type) => Game, (game) => game.participations)
  game: Game
  @Column({ nullable: false })
  gameUuid: string

  @Field((type) => [String])
  @Column('jsonb', { default: [] })
  answers: string[]

  @Field((type) => [Number])
  @Column('jsonb', { default: [] })
  points: number[]

  @Field(() => Date)
  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date

  @Field(() => Date)
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date
}
