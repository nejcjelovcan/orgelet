import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm'
import { ObjectType, Field, ID, registerEnumType } from 'type-graphql'

import IGame, {
  GameType,
  GameArea,
  GameState,
  GameOrder,
} from 'orgelet-core/src/model/IGame'

import GameParticipation from './GameParticipation'

registerEnumType(GameType, { name: 'GameType' })

registerEnumType(GameArea, { name: 'GameArea' })

registerEnumType(GameState, { name: 'GameState' })

registerEnumType(GameOrder, { name: 'GameOrder' })

@Entity()
@ObjectType()
export default class Game extends BaseEntity implements IGame {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  uuid: string

  @Field((type) => GameType)
  @Column({ nullable: false, default: GameType.Countries })
  type: GameType

  @Field((type) => GameArea)
  @Column({ nullable: false, default: GameArea.World })
  area: GameArea

  @Field((type) => GameState)
  @Column({ nullable: false, default: GameState.Idle })
  state: GameState

  @Field((type) => GameOrder)
  @Column({ nullable: false, default: GameOrder.Random })
  order: GameOrder

  @Field()
  @Column({ nullable: false, default: 10 })
  length: number

  @Field(() => [GameParticipation], { nullable: true })
  @OneToMany((type) => GameParticipation, (participation) => participation.game)
  participations: GameParticipation[]

  @Field((type) => [String])
  @Column('jsonb', { default: [] })
  targets: string[]

  @Field(() => Date)
  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date

  @Field(() => Date)
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date
}
