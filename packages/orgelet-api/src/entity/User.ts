import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm'
import { ObjectType, Field, ID } from 'type-graphql'

import IUser from 'orgelet-core/src/model/IUser'

import GameParticipation from './GameParticipation'

@Entity()
@ObjectType()
export default class User extends BaseEntity implements IUser {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  uuid: string

  @Field(() => String)
  @Column({ nullable: false })
  alias: string

  @Field(() => [GameParticipation])
  @OneToMany(
    (type) => GameParticipation,
    (participation) => participation.participant,
  )
  participations: GameParticipation[]

  @Field(() => String)
  token?: String

  @Field(() => Date)
  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date

  @Field(() => Date)
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date
}
